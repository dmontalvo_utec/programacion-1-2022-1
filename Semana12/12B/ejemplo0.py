def factorialI(n):
    print(f"Ejecutando FactorialI({n})...")
    resultado = 1
    while n > 1:
        resultado = resultado * n
        n -= 1

    return resultado


def factorialR(n):
    print(f"Ejecutando FactorialR({n})...")
    if n <= 1:
        return 1
    else:
        return n * factorialR(n - 1)


if __name__ == '__main__':
    print("Factorial de 6(Iterativo) :", factorialI(6))
    print()
    print("Factorial de 6(Recursivo) :", factorialR(6))
