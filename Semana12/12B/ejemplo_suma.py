"""
Suma recursiva

4 + 3 = 4 + 1 + 1 + 1  TODO implementarte
      = 1 + 1 + 1 + 4
"""


def sumaR(m, n):
    if n == 0:
        return m
    else:
        return 1 + sumaR(m, n - 1)


if __name__ == '__main__':
    print("4 + 5 recursivo = ", sumaR(4, 5))
    print("10 + 20 recursivo = ", sumaR(10, 20))
