def maximo(lista):
    print(f"Maximo de {lista}")
    if len(lista) == 1:
        return lista[0]
    else:
        return max(lista[0], lista[1:])


if __name__ == '__main__':
    # print(maximo([]))
    print(maximo([2]))
    print(maximo([4, 3]))
    print(maximo([3, 7, 5, 1]))
    print(maximo([3, 7, 5, 1, 8]))
