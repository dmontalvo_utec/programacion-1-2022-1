"""
Algoritmo recursivo: Fibonacci
"""


def fibonacciI(n):
    a, b = 0, 1
    for i in range(n):
        a, b = b, a + b
    return a


def fibonacciR(n):
    print(f"->fibR({n})")
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fibonacciR(n - 1) + fibonacciR(n - 2)


if __name__ == '__main__':
    print("Fibonacci iterativo. Fib(7) = ", fibonacciI(7))
    print("Fibonacci recursivo. Fib(7) = ", fibonacciR(7))
