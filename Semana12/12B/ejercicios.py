import time


def multiplicaR(m, n):
    if n == 1:
        return m
    else:
        return m + multiplicaR(m, n - 1)


def potenciaR(m, n):
    if n == 1:
        return m
    else:
        return m * potenciaR(m, n - 1)


def suma_digitos(n):
    if n < 10:
        return n
    else:
        return n % 10 + suma_digitos(n // 10)


def sum_cuadrados(n):
    if n == 1:
        return 1
    else:
        return n * n + sum_cuadrados(n - 1)


def inverso(s: str):
    if len(s) <= 1:
        return s
    else:
        return s[-1] + inverso(s[1:-1]) + s[0]


def cuenta_regresiva(n):
    time.sleep(.2)
    if n == 1:
        print(n)
    else:
        print(n, end="...")
        cuenta_regresiva(n - 1)


if __name__ == '__main__':
    print("4x5 recursivo = ", multiplicaR(4, 5))
    print("2^3 recursivo = ", potenciaR(2, 3))

    print("suma digitos 1234= ", suma_digitos(1234))
    print("suma digitos 2323= ", suma_digitos(2323))

    print("suma cuadrados hasta 3 = ", sum_cuadrados(3))
    print("suma cuadrados hasta 5 = ", sum_cuadrados(5))

    print("invirtiendo perrito -> ", inverso("perrito"))
    print("invirtiendo programacion -> ", inverso("programacion"))

    cuenta_regresiva(10)
