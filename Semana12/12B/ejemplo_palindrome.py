def es_palindrome(s):
    print(f"es_palindrome('{s}'")
    if len(s) == 0:
        return True
    else:
        return s[0] == s[-1] and es_palindrome(s[1:-1])


#  A B B A
#  R E C O N O C E R
#  S O M E T E M O S
# 10:11/01 TODO recursivo con simbolos

if __name__ == '__main__':
    print("mama es pali? :", es_palindrome("mama"))
    print("maam es pali? :", es_palindrome("maam"))
    print("sometemos es pali? :", es_palindrome("sometemos"))
    print("reconocer es pali? :", es_palindrome("reconocer"))
