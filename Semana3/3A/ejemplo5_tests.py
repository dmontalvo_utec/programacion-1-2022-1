import unittest
from subprocess import Popen, PIPE


class Ejercicio5TestCase(unittest.TestCase):

    def test_denominacion_100(self):
        p = Popen(["python", "ejemplo5.py"], stdin=PIPE, stdout=PIPE, encoding='utf8')
        out = p.communicate("100")
        self.assertTrue(out[0].endswith("Benjamin Franklin\n"))

    def test_denominacion_10(self):
        p = Popen(["python", "ejemplo5.py"], stdin=PIPE, stdout=PIPE, encoding='utf8')
        out = p.communicate("10")
        self.assertTrue(out[0].endswith("Alexander Hamilton\n"))

    def test_denominacion_50(self):
        p = Popen(["python", "ejemplo5.py"], stdin=PIPE, stdout=PIPE, encoding='utf8')
        out = p.communicate("50")
        self.assertTrue(out[0].endswith("Ulysses S\n"))

    def test_denominacion_500(self):
        p = Popen(["python", "ejemplo5.py"], stdin=PIPE, stdout=PIPE, encoding='utf8')
        out = p.communicate("500")
        self.assertTrue(out[0].endswith("Denominación descontinuada\n"))

    def test_denominacion_1000(self):
        p = Popen(["python", "ejemplo5.py"], stdin=PIPE, stdout=PIPE, encoding='utf8')
        out = p.communicate("1000")
        self.assertTrue(out[0].endswith("Denominación descontinuada\n"))

    def test_denominacion_0(self):
        p = Popen(["python", "ejemplo5.py"], stdin=PIPE, stdout=PIPE, encoding='utf8')
        out = p.communicate("0")
        self.assertTrue(out[0].endswith("No existe esa denominación\n"))

    def test_denominacion_55(self):
        p = Popen(["python", "ejemplo5.py"], stdin=PIPE, stdout=PIPE, encoding='utf8')
        out = p.communicate("55")
        self.assertTrue(out[0].endswith("No existe esa denominación\n"))
