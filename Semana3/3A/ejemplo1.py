"""
Elaborar un algoritmo y un programa en Python que solicite el nombre y la edad de una persona.
Deberá imprimir un saludo: “Hola <nombre>.”.
Además, otro mensaje si la persona tiene 18 años: “Usted debe tramitar ante el RENIEC su DNI mayor de edad.”

"""
nombre = input("Ingrese su nombre : ")
edad = int(input("Ingrese su edad : "))
print("Hola", nombre)

if edad == 18:
    print("Usted debe tramitar ante RENIEC su DNI de mayor de edad.")
