"""
Escribir un programa en Python que permita ingresar un número del 1-7 y muestre su equivalente en letras.
Ejemplo 1=Lunes, 2 Martes, etc., si esta fuera de rango mostrar el mensaje error : "Número inválido"
"""
num = int(input("Ingrese un número del 1-7 : "))

if 0 < num < 8:
    if num == 1:
        print("Lunes")
    elif num == 2:
        print("Martes")
    elif num == 3:
        print("Miercoles")
    elif num == 4:
        print("Jueves")
    elif num == 5:
        print("Viernes")
    elif num == 6:
        print("Sabado")
    else:
        print("Domingo")
else:
    print("Numero no valido")
