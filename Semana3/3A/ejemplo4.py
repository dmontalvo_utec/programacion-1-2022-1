"""
Realice un programa que permita leer la edad de una persona y el programa determine el costo de la entrada al cine,
 según la tabla:

|  Edad   | Precio en Soles |
| 0 - 17  |     15          |
| 18 - 30 |     25          |
| 31 - 45 |     30          |
| 46 a más|     40          |

"""

edad = int(input("Ingrese su edad : "))

print(" ** Cinestar ** ".center(20))

if 0 <= edad <= 17:
    precio = 15
elif edad>=18 and edad <=30:
    precio = 25
elif 31 <= edad <= 45:
    precio = 30
else:
    precio = 40

#TODO validar nros positivos

print(f"Usted debe pagar S/{precio}".center(20))
