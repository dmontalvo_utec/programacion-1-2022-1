"""
Cada denominación monetaria del dólar tiene asociado un personaje con algún tipo de relevancia histórica,
usualmente presidentes y héroes.

Aunque existen denominaciones de 500, 1000, 5000 y 10,000 para uso público estas denominaciones
han sido descontinuadas en 1969, A continuación, mostramos la lista vigente:

|    Personaje      | Denominación
|George Washington  |  1
|Thomas Jefferson   |  2
|Abraham Lincoln    |  5
|Alexander Hamilton | 10
|Andrew Jackson     | 20
|Ulysses S. Grant   | 50
|Benjamin Franklin  | 100

Escribir un programa que lea la denominación y que muestre :
- el nombre del personaje
- en el caso de los billetes de 500, 1000, 5000 y 10000, deberá mostrarse el mensaje “Denominación descontinuada”
- otros valores deberá mostrar  “No existe esa denominación”.
"""

billete = int(input("Ingrese denominacion en $$ : "))

if billete == 1:
    print("George Washington")
elif billete == 2:
    print("Thomas Jefferson")
elif billete == 5:
    print("Abraham Lincoln")
elif billete == 10:
    print("Alexander Hamilton")
elif billete == 20:
    print("Andrew Jackson")
elif billete == 50:
    print("Ulysses S")
elif billete == 100:
    print("Benjamin Franklin")
elif billete in [500, 1000, 5_000, 10_000]:
    print("Denominación descontinuada")
else:
    print("No existe esa denominación")
