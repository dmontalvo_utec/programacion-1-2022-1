"""
Elaborar un algoritmo que evalúe una nota aprobatoria >= 11 e imprima un mensaje de felicitación.
"""

nota = int(input("Ingrese nota : "))

if nota >= 11:
    print("Felicitaciones")
else:
    print("Sigue estudiando!")
