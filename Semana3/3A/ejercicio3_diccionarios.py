"""
OJO. Esto es un ADELANTO. No se calificará el uso de diccionarios hasta la semana correspondiente


Escribir un programa en Python que permita ingresar un número del 1-12 y muestre el mes que
corresponde y la estación a la que pertenece.
Ejemplo 1=Enero, Verano
"""
meses = {1: "Enero", 2: "Febrero", 3: "Marzo", 4: "Abril", 5: "Mayo", 6: "Junio",
         7: "Julio", 8: "Agosto", 9: "Septiembre", 10: "Octubre", 11: "Noviembre", 12: "Diciembre"}

estaciones = {1: "Verano", 2: "Verano", 3: "Verano", 4: "Otoño", 5: "Otoño", 6: "Otoño",
              7: "Invierno", 8: "Invierno", 9: "Invierno", 10: "Primavera", 11: "Primavera", 12: "Primavera"}

n = int(input("Ingrese un número del 1 al 12 : "))

print(f"{meses[n]}, {estaciones[n]}")
