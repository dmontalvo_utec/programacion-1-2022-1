"""
Escribir un programa en Python que permita ingresar un número del 1-12 y muestre el mes que
corresponde y la estación a la que pertenece.
Ejemplo 1=Enero, Verano
"""
n = int(input("Ingrese un número del 1 al 12 : "))

if 1 <= n <= 12:
    if n == 1:
        print("Enero, Verano")
    elif n == 2:
        print("Febrero, Verano")
    elif n == 3:
        print("Marzo, Verano")
    elif n == 4:
        print("Abril, Otoño")
    elif n == 5:
        print("Mayo, Otoño")
    elif n == 6:
        print("Junio, Otoño")
    elif n == 7:
        print("Julio, Invierno")
    elif n == 8:
        print("Agosto, Invierno")
    elif n == 9:
        print("Septiembre, Invierno")
    elif n == 10:
        print("Octubre, Primavera")
    elif n == 11:
        print("Noviembre, Primavera")
    else:
        print("Diciembre, Primavera")
else:
    print("Número incorrecto")
