"""
Escribir un programa que permita leer como dato un número entero y luego imprima si es positivo, negativo o es cero.
"""

dato = int(input("Ingrese un número : "))

if dato > 0:
    print("Positivo")
elif dato < 0:
    print("Negativo")
else:
    print("Es cero")
