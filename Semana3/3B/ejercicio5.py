"""
Modifique el programa del juego en Python, con la finalidad que cuando imprima el mensaje “No adivinó. Siga intentando.”
adicionalmente, también imprima una ayuda señalando: “El número a adivinar es menor” o “El número a adivinar es mayor”
"""
import random

numero = random.randint(1, 10)
intento = 0
while intento != numero:
    intento = int(input("Intente adivinar el número: "))
    if intento == numero:
        print("Felicitaciones adivinó el número!!!")
    else:
        print("No adivinó. Siga intentando.")
        if intento > numero:
            print("El número a adivinar es menor")
        else:
            print("El número a adivinar es mayor")
