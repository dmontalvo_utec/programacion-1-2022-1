"""
Desarrolle un programa que imprima la tabla de multiplicar de un número n ingresado por teclado.
"""

n = int(input("Ingrese N : "))
i = 1

print(f"La tabla del {n}")
while i <= 12:
    print(f"{n} x {i} = {n * i}")
    i += 1
