"""
Diseñe e implemente un algoritmo en Python que obtenga el promedio de un conjunto de números ingresados por el usuario.
El usuario ingresará cero para indicar que ya no ingresará más números. El cero no se considera en el promedio.
"""

prom = 0
dato = int(input("Ingrese un dato: "))
contador = 1

while dato != 0:
    prom = prom + dato
    contador += 1
    dato = int(input(" Ingrese un dato: "))

print("El prromedio es ", prom)
