"""
Dada la serie: 1, 4, 9, 16, 25, 36, ...
Diseñe e implemente un algoritmo en Python que permita al usuario ingresar la cantidad de números a mostrar.
El algoritmo debe imprimir la secuencia: 1, 4, 9, 16, 25, 36, ..., hasta el cuadrado del número n.

"""

n = int(input("Ingrese N : "))

i = 1

while i <= n:
    print(i **2, end=",")
    i += 1
