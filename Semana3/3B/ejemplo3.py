"""
Diseñe un algoritmo e implemente un programa en Python que permita jugar : “Adivina el número”.
El programa seleccionará un número aleatorio entre 1 y 10 y preguntará
al usuario que ingrese  un número hasta que lo adivine.

"""
import random

numero = random.randint(1, 10)
intento = 0
while intento != numero:
   intento = int(input("Intente adivinar el número: "))
   if intento == numero:
       print("Felicitaciones adivinó el número!!!")
   else:
       print("No adivinó. Siga intentando.")