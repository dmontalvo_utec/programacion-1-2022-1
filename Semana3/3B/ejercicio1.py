"""
Diseñe e implemente un algoritmo en Python que permita al usuario ingresar un número,
y el algoritmo debe imprimir los números pares, desde cero hasta el número ingresado.
"""
num = int(input("Ingrese un número entero positivo : "))

i = 0
while i <= num:
    if i % 2 == 0:
        print(i, end=",")

    i = i + 1
    # Que tendria que cambiar para usar i=i+2?
