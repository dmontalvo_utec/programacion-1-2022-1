def abrir_archivo(nombre, modo="w"):
    return open(nombre, modo)
    # with open(archivo,"w") as file:


if __name__ == '__main__':
    print("\nEscribiendo\n")
    file = abrir_archivo("programacion.txt", "w+")
    file.write("Python\n")
    file.write("variables\n")
    file.write("Condicionales\n")
    file.write("Repetitivas\n")
    file.write("Funciones\n")

    print("\nLeyendo con for\n")
    file = abrir_archivo("programacion.txt", "r")
    for linea in file:
        print(linea, end="")

    print("\nLeyendo con while y readline\n")
    file.seek(0)
    linea = file.readline()
    while linea != '':
        print(linea, end="")
        linea = file.readline()

    print("\nLeyendo con readlines\n")
    file.seek(0)
    print(file.readlines())

    print("\nEscribiendo al final\n")
    file = abrir_archivo("programacion.txt", "a")
    file.write("Se viene progra 2\n")
    file.write("Compilador vs interprete\n")
    file.write("Librerias en C++\n")
