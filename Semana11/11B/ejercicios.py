def contar_lineas(nombre_archivo):
    """
    Ejercicio 1
    Implemente un algoritmo que permita contar cuántas líneas tiene un archivo.
    :param nombre_archivo:
    :return nro de lineas del archivo
    """
    pass


def contar_palabras(nombre_archivo):
    """
    Ejercicio 2
    Implemente un algoritmo que permita contar cuántas palabras tiene un archivo.
    :param nombre_archivo:
    :return nro palabras del archivo
    """
    pass


def copiar(original, copia):
    """
    Ejercicio 3
    Implemente un algoritmo que permita copiar un archivo a otro.
    :param original: nombre del archivo original
    :param copia: nombre del archivo a copiar
    :return: archivo copiado
    """
    pass


def recibir_texto(nombre_archivo):
    """
    Ejercicio 4
    Implemente un algoritmo recibir texto desde el teclado hasta que se ingrese la palabra “fin”.
    Cada texto ingresado deberá ser guardado en un archivo.
    :param nombre_archivo: nombre del archivo a escribir
    :return: archivo creado
    """
    pass


def leer_alreves(nombre_archivo):
    """
    Ejercicio 5
    Implemente un algoritmo que permita leer un archivo y escribirlo en otro pero al revés, es decir la última línea del archivo origen será la primera en el archivo destino
    :param nombre_archivo:
    :return:
    """
    pass


if __name__ == '__main__':
    nombre_archivo = "ejercicio"  # 5 lineas 13 palabras

    l = contar_lineas(nombre_archivo)
    print(f"El archivo tiene {l} lineas")

    p = contar_palabras(nombre_archivo)
    print(f"El archivo tiene {p} palabras")

    copia = copiar(nombre_archivo, "copia_ejercicio")
    print(len(copia.read_lines()), "lineas copiadas")

    recibir_texto("input.txt")

    leer_alreves("input.txt")
