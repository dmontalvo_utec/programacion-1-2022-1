import random


# print(__name__)


def crear_diccionario():
    """
    Crear un diccionario vacio
    :return: empty diccionary
    """
    # result = dict()
    result = {}
    return result


def llenar_diccionario(data: dict):
    """
    Crear un diccionario vacio
    :return: empty diccionary
    """
    for i in range(10):
        key = "ID" + str(i)
        data[key] = random.randint(0, 20)


def llenar_diccionario_usuario(data: dict):
    """
    llena un diccionario vacio
    :return: None
    """
    i = 0
    while i < 10:
        key = input(f"Ingrese llave {i + 1} : ")
        if key not in data.keys():
            i += 1
            data[key] = random.randint(0, 20)


def cambiar_a_0(valor):
    if type(valor) is list:
        valor.append(2)


def escribir(texto):
    print(texto, "MUNDO")


if __name__ == '__main__':
    d = crear_diccionario()
    print(d)
    a = escribir("HOLA")
    print(a)

    llenar_diccionario_usuario(d)
    print(d)
