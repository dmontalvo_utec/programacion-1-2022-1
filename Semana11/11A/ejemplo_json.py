import json

import ejemplo_apis


# json a str. dumps
def diccionario_json(data):
    return json.dumps(data, indent=4)


# diccionario a archivo. dump
def diccionario_archivo_json(data, file_name):
    with open(file_name, "w") as file:
        json.dump(data, file)


# archivo a diccionario
def archivo_diccionario(file_name):
    with open(file_name, "r") as file:
        return json.load(file)


if __name__ == '__main__':
    # Esto es un diccionario
    print("1. Data Peru desde API")
    data_peru = ejemplo_apis.data_pais("PE")
    print(data_peru[0]['name']['official'])

    print("2. Escribiendo diccionario a json")
    json_str = diccionario_json(data_peru)
    print(json_str)

    print("3. Escribiendo diccionario a archivo como JSON")
    diccionario_archivo_json(data_peru, "data_peru.json")

    print("4. Leyendo diccionario desde archivo JSON")
    data_from_file = archivo_diccionario("data_peru.json")
    print(data_from_file[0]['name']['official'])
