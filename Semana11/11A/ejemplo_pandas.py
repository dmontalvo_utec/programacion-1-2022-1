import matplotlib.pyplot as plt
import pandas as pd


def separador(texto):
    print(f"\n{'-' * 100}\n{texto}\n{'-' * 100}")


if __name__ == '__main__':
    # lee el archivo CSV
    df = pd.read_csv("covid-19-peru-camas-uci.csv")

    # muestra las primeras filas
    separador("Primeras filas")
    print(df.head())

    # cabeceras
    separador("Filas [0,2>")
    print(df[0:2])

    # columnas
    separador("Columnas fecha, estado, minsa")
    print(df[["fecha", "estado", "minsa"]])

    separador("Filtrando")
    filtro = df['estado'] == "en uso"
    df_enuso = df.where(filtro)
    print(df_enuso)

    separador("Agrupando")
    grupo = df.groupby('estado')
    print(grupo.describe())

    separador("Agrupando y filtrando")
    grupo_filtro = df.groupby('estado')['minsa']
    print(grupo_filtro.describe())

    separador("Gráficos")
    grupo_filtro_sum = df.groupby('estado')['minsa'].sum()
    plt.close("all")
    grupo_filtro_sum.plot(kind='bar')
    plt.show()
