import json

import requests


def mostrar_json(objeto_json):
    dump = json.dumps(objeto_json, indent=2)
    print(type(dump))
    print(dump)


def json_diccionario(objeto_json):
    return json.load(objeto_json)


def actividad():
    request = requests.get("https://www.boredapi.com/api/activity", verify=False)
    return request.json()


def nacionalidad(nombre):
    request = requests.get(f"https://api.nationalize.io?name={nombre}", verify=False)
    return request.json()


def universidades(pais):
    request = requests.get(f"http://universities.hipolabs.com/search?country={pais}", verify=False)
    return request.json()


def usuario(id):
    request = requests.get(f"https://reqres.in/api/users/{id}", verify=False)
    return request.json()


def data_pais(code):
    request = requests.get(f"https://restcountries.com/v3/alpha/{code}", verify=False)
    return request.json()


if __name__ == '__main__':
    respuesta = data_pais("PE")
    print(type(respuesta))

    print(respuesta)
    mostrar_json(respuesta)
