"""
Escriba un programa que permita registrar una lista de cursos en un archivo JSON.
El curso debe tener los siguientes campos:  nombre, carrera, ciclo, créditos, docente.
El campo “docente” es también un diccionario que contiene los siguientes campos: nombre, correo y horario_oficina.

"""