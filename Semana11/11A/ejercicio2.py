"""
Usando la información de la API https://reqres.in/api/users?page=1
Se le pide implementar un programa que busque los datos completos del usuario “Emma” (first_name)
Luego generalice la búsqueda para cualquier valor del campo.

"""