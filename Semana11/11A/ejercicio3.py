"""
Se le pide implementar un programa entregue el nombre completo de una país recibiendo como input el código abreviado de dicho país.

Para ello, debe ayudarse de la siguiente API:
	https://restcountries.com/v3/alpha/[codigo]

 Ejemplo, si se quiere saber el nombre del país con código “co”, hacer lo siguiente:
https://restcountries.com/v3/alpha/co

Y mostrar el campo name/official

"""
