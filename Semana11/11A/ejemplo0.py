import json

import yaml


def mostrar(valor, enable_yaml=False):
    if yaml:
        print(yaml.dump(valor, default_flow_style=False))
    else:
        print(json.dumps(valor, indent=4))


if __name__ == '__main__':
    # Creando diccionario anidado (mismas llaves, llaves mixtas)
    diccionario = {
        1: {
            '10': 520,
            '20': 320
        },
        2: {
            'data': [1, 20, 50, 20],
            'flow': "1->4->5->6"
        }

    }
    mostrar(diccionario)

# Alumnos, Data Curso,


# Agregar 1x1 o todo un diccionario

#Eliminar datos con del



#Iterando