# Enteros y strings (suma y multiplicacion)
edad = 25
print(edad * 5)
edad = "25"
print(edad * 5)
print("Un String es de type :", type(edad))

# caracteres especiales
print("Me gustan los helados D'onofrio")
print("Programación es un curso \"difícil\"")
print("Pero en el LAB 202\ntodos quieren aprobar\tLa esperanza es lo último que se pierde!!")

# tipos de datos
print("enteros")
numero = 0b111
print(numero)
numero = 0o66
print(numero)
numero = 0x152B
print(numero)
print("Las variables tipo entero retornan type : ", type(numero))

print("decimales")
decimal = 3.141592
print(decimal)
decimal = 15e5
print(decimal)
decimal = 15e-5
print(decimal)
decimal = float(159)
print(decimal)
decimal = float("3.141592")
print(decimal)
print("Las variables tipo decimal retornan type : ", type(decimal))

print("bool")
b = bool(0)
print(b)
b = bool(6)
print(b)
b = bool(-5)
print(b)
b = bool(1.56)
print(b)
b = bool("Prueba")
print(b)
print("Las variables tipo lógica retornan type : ", type(b))

print("strings")
