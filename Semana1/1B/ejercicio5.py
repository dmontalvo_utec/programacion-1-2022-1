# Escriba un programa que reciba un número representando una cantidad de segundos.
# Posteriormente, el programa deberá calcular cuantos minutos y segundos hay en ese tiempo.
# Por ejemplo: 150 segundos -> 2 minutos 30 segundos.

totalSeg = int(input("Ingrese una cantdad de segundos : "))

horas = totalSeg // 3600
min = totalSeg % 3600

seg = min % 60
min = min // 60

print(f"{totalSeg} segundos equivalen a {horas} horas, {min} minutos y {seg} segundos")
