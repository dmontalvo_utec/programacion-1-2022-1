# operadores aritmeticos
print("**** OPERADORES ARITMETICOS ****")
print("Suma. 5+2 : ", 5 + 2)
print("Suma. 5+7.5 : ", 5 + 7.5)
print("Resta. 5-2 : ", 5 - 2)
print("Resta. 5.2-2 : ", 3.2)
print("Mult. 5.2*10 : ", 5.2 * 10)
print("Mult. 5*2 : ", 5 * 2)
print("División. 5/2 : ", 5 / 2)
print("División. 5.2/2 : ", 5.2 / 2)
print("División entera. 5//2 : ", 5.2 // 2)
print("Modulo(Residuo). 5%2 : ", 5 % 2)
print("Modulo(Residuo). 5.3%2 : ", 5.3 % 2)
print("Potencia. 2^5 : ", 2 ** 5)
print("Potencia. 2.0^5 : ", 2.0 ** 5)

# operadores con cadenas
print("**** OPERADORES CADENAS ****")
a = "UTEC"
b = "Ninja"
c = "Developers"

mensaje = a + " " + b + c
print(mensaje)
print(mensaje[0])
print(mensaje[1])
print(mensaje[5:])
print("To be continued...")

# Precedencia de operadores (** +,/,//, %, + - )
print("**** PRECEDENCIA ****")
print(3 * 5 + 1)
print(3 ** 2 * (5 + 1))
