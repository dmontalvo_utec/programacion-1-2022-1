# Escribir un programa en Python que permita hallar el área y el volumen de una esfera.
# Área = 4 * 3.1415 * r^2
# Volumen = 4/3 * PI * r^3
import math

radio = float(input("Ingrese el radio de la esfera : "))

area = 4 * math.pi * radio ** 2
volumen = 4 / 3 * math.pi * radio ** 3

print(f"El área de la esfera es {area} y el volumen es {volumen}")
print("Redondeando.....")

print(f"El área de la esfera es {round(area, 4)} y el volumen es {round(volumen, 4)}")
