# Escribir un programa en Python que permita ingresar un monto en soles y muestre su equivalente en dólares y euros
# TC USD = 3.74
# TC EUR = 4.11

soles = float(input("Ingrese el monto en soles : "))

euros = soles * 4.11
dolares = soles * 3.74

print(f"{soles} PEN equivalen a {euros} EUR y {dolares} USD")
