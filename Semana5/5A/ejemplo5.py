"""
Desarrolle un programa que permita leer como dato un número cuyo rango puede estar desde 1 a 9.
Este dato representa la cantidad de filas que tendrá un rectángulo formado por números.

El programa:
    - Debe verificar el ingreso del número de filas. Este dato deberá ser desde 1 a 9.
    - Imprime un rectángulo formado por números tal y como se muestra en los ejemplos.

Ejemplo 2:

Filas: 7
0000007
0000076
0000765
0007654
0076543
0765432
7654321
"""

while True:
    filas = int(input("Número de filas : "))
    if 1 <= filas <= 9:
        break

i = 1
while i <= filas:
    j = 1
    while j <= filas - i:
        print("0", end=" ")
        j += 1
    k = filas
    while k >= filas - i + 1:
        print(k, end= " ")
        k -= 1
    print()
    i += 1
