"""
Realice un programa que permita leer como dato un número mayor a 15
y el programa imprima el valor del factorial de cada número desde el 1 hasta el número que se ingresó como dato.
"""

while True:
    filas = int(input("Número de filas : "))
    if filas > 15:
        break

i = 1

while i <= 15:
    print(i, end=" ")
    factorial = 1
    j = 1
    while j <= i:
        factorial *= j
        j += 1
    print(factorial)
    i += 1
