"""
Realice un programa que lea un número cuyo rango de valores esté entre 1 y 9 y el programa imprima un triángulo como se muestra en el ejemplo:

Filas : 4

1234
123
12
1

"""

while True:
    filas = int(input("Número de filas : "))
    if 1 <= filas <= 9:
        break

i = 1
while i <= filas:
    j = 1
    while j <= filas - i + 1:
        print(j, end="")
        j += 1
    i += 1
    print()
