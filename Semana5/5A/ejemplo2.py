'''
Realice un programa que permita leer como dato un número entre 1 y 9,
que representa la cantidad de filas que tendrá un árbol construido como se indica en el ejemplo:
'''

while True:
    filas = int(input("Número de filas : "))
    if 1 <= filas <= 9:
        break

columnas = 1
i = 1
while i <= filas:
    j = 1
    while j <= i:
        print(j, end="")
        j += 1
    i += 1
    print()
