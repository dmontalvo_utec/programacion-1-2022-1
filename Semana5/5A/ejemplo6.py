"""
En el planeta Gorinki, la presión sistólica de los gorinkianos oscila entre 90 y 130 como valores normales.
Debido a la pandemia los gorinkianos han sufrido mucho y se ha reportado una subida en el caso de hipertensos.
Para determinar si un gorinkiano es hipertenso, se le toma la presión sistólica 5 veces al dīa
y si el promedio esta por encima de 130 se considera hipertenso.

Realice un programa que permita leer el número de gorinkianos a los cuales se hará el test
para determinar si son o no hipertensos.

El programa:
 - Debe verificar el ingreso del número de gorinkianos. Este dato deberá ser mayor a 3
 - Luego leer para cada gorkiniano, los valores de las 5 pruebas de presión sistólica, hallar el promedio y determinar finalmente si es hipertenso o no.

"""

nro = int(input("Nro de gorkianos : "))
while nro <= 3:
    nro = int(input("Nro de gorkianos : "))

i = 1
while i <= nro:
    j = 1
    suma = 0
    while j <= 5:
        presion = float(input(f"Presion {j} : "))
        suma += presion
        j += 1
    if suma / 5 > 130:
        print(f"Ud. es hipertenso. Su presión sistólica es de {suma / 5}")
    else:
        print(f"Ud. NO es hipertenso. Su presión sistólica es de {suma / 5}")
