'''
Cuál es el re4sultado de estos bucles anidados?
'''

ancho = int(input("Ancho : "))
alto = int(input("Alto  :"))
cfilas = 1

while cfilas <= alto:
    cc = 1
    while cc <= ancho:
        print(" # ", end="")
        cc = cc + 1
    print("")
    cfilas = cfilas + 1
