import random


def crearMatriz(filas, columnas):
    matriz = []
    for i in range(filas):
        matriz.append([0] * columnas)
    return matriz


def mostrarMatriz(matriz):
    print()
    for i in range(len(matriz)):
        for j in range(len(matriz[i])):
            print(str(matriz[i][j]).rjust(2), end=" ")
        print()
    print()


def generarAletaorio(matriz, mini, maxi):
    for i in range(len(matriz)):
        for j in range(len(matriz[i])):
            matriz[i][j] = random.randint(mini, maxi)


def sumarColumna(matriz, co):
    suma = 0
    for i in range(len(matriz)):
        suma += matriz[i][co]
    return suma


def buscarNegativos(matriz):
    flag = False
    for fila in matriz:
        for cell in fila:
            if cell < 0:
                flag = True
                return flag
    return flag


if __name__ == '__main__':
    filas = 4
    columnas = 6
    M = []

    M = crearMatriz(filas, columnas)
    mostrarMatriz(M)
    generarAletaorio(M, -1, 9)
    mostrarMatriz(M)

    print("Suma columna 2 =", sumarColumna(M, 1))
    print("Negativos :", buscarNegativos(M))
