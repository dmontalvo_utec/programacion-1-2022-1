"""sobre el ejemplo 1"""
import caso1


def contarNegativos(matriz, fila):
    """
    Cuenta negativos en determinada fila
    """
    con = 0
    for j in range(len(matriz[fila])):
        if matriz[fila][j] < 0:
            con += 1
    return con


def filasZero(matriz):
    """
    Retorna una lista con las filas que tienen al menos un CERO
    :param matriz:
    :return:
    """
    filas = []
    for i in range(len(matriz)):
        for j in range(len(matriz[i])):
            if matriz[i][j] == 0:
                filas.append(i)
                break
    return filas


def sumarDiagonales(matriz):
    """
    suma las diagonales
    :return: -1 si las filas y las columnas no son iguales
    """

    if len(matriz) != len(matriz[0]):
        return -1

    size = len(matriz)

    suma = 0
    for i in range(size):
        for j in range(size):
            if i == j or j == size - i - 1:
                suma += matriz[i][j]
    return suma


def columnaValorMaximo(matriz):
    max = -2
    maxCol = -1
    for i in range(len(matriz)):
        for j in range(len(matriz[i])):
            if matriz[i][j] > max:
                max = matriz[i][j]
                maxCol = j
    return maxCol


if __name__ == '__main__':
    matriz = caso1.crearMatriz(5, 5)
    caso1.generarAletaorio(matriz, -2, 9)
    caso1.mostrarMatriz(matriz)

    fila = 3
    print(f"Nro de negativos fila {fila}:", contarNegativos(matriz, fila))
    print(f"Filas que tienene 0:", filasZero(matriz))
    print(f"Suma diagonales matriz:", sumarDiagonales(matriz))
    print(f"Columna con valor máximo:", columnaValorMaximo(matriz))
