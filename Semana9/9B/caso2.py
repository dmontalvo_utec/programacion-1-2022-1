def crearPanel(filas, columnas):
    # creacion de la matriz
    M = []
    for i in range(filas):
        M.append([0] * columnas)
    return M


def crearDigitos():
    d1 = [
        [' ', ' ', 'x', ' ', ' '],
        [' ', 'x', 'x', ' ', ' '],
        ['x', ' ', 'x', ' ', ' '],
        [' ', ' ', 'x', ' ', ' '],
        [' ', ' ', 'x', ' ', ' '],
        [' ', ' ', 'x', ' ', ' '],
        ['x', 'x', 'x', 'x', 'x']
    ]
    d2 = [
        ['x', 'x', 'x', 'x', 'x'],
        [' ', ' ', ' ', ' ', 'x'],
        [' ', ' ', ' ', ' ', 'x'],
        ['x', 'x', 'x', 'x', 'x'],
        ['x', ' ', ' ', ' ', ' '],
        ['x', ' ', ' ', ' ', ' '],
        ['x', 'x', 'x', 'x', 'x']
    ]

    return d1, d2


def mostrarDigito(digito):
    print('')
    for i in range(len(digito)):
        for j in range(len(digito[0])):
            print(digito[i][j], end=' ')
        print('')


# MAIN
if __name__ == '__main__':
    filas = 7
    columas = 7 * 4
    M = crearPanel(filas, columas)
    d1, d2 = crearDigitos()

    mostrarDigito(d1)
    mostrarDigito(d2)
