# Creando una matriz 0's


# Acceso a elementos


# Mostrar pbi


def mostrarPBI():
    paises, datos = pbi.crearMatrizPBI()

    print("%10s 2017  2018" % " ")
    for i, pais in enumerate(paises):
        print("%10s" % pais, end=" ")
        for j in range(2):
            print("{0:.1f}".format(datos[i][j]), end="  ")
        print()


if __name__ == '__main__':
    mostrarPBI();
