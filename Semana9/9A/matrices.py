def crearMatrizPBI():
    paises = 11
    anios = 2

    paises = ["Argentina", "Bolivia", "Brasil", "Chile",
              "Colombia", "Ecuador", "Mexico", "Paraguay",
              "Perú", "Uruguay", "Venezuela"]
    pbi = [
        [2.9, 2.5],
        [3.9, 4.0],
        [0.9, 2.2],
        [1.5, 3.3],
        [1.8, 2.6],
        [1.0, 2.0],
        [2.2, 2.3],
        [4.0, 4.0],
        [2.5, 3.5],
        [3.0, 3.0],
        [-9.5, -8.5]
    ]

    return paises, pbi


def crearMatrizVenta():
    divisiones = ["Línea Blanca", "Electrodomésticos", "Juguetería",
                  "Perecibles", "Limpieza"]
    datos = [[450, 650, 342], [340, 487, 767], [134, 212, 354],
             [180, 464, 565], [647, 324, 232]]

    return divisiones, datos
