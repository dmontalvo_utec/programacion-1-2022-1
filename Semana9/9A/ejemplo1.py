n_paises = 11
n_anios = 2

paises = ["Argentina", "Bolivia", "Brasil", "Chile",
          "Colombia", "Ecuador", "Mexico", "Paraguay",
          "Perú", "Uruguay", "Venezuela"]
pbi = [
    [2.9, 2.5],
    [3.9, 4.0],
    [0.9, 2.2],
    [1.5, 3.3],
    [1.8, 2.6],
    [1.0, 2.0],
    [2.2, 2.3],
    [4.0, 4.0],
    [2.5, 3.5],
    [3.0, 3.0],
    [-9.5, -8.5]
]


def mostrarDatos():
    print("%10s%6s%6s" % (" ", "2017", "2018"))
    for i in range(n_paises):
        print("%10s" % paises[i], end="   ")
        for j in range(n_anios):
            print("{0:.1f}".format(pbi[i][j]), end=" ")
        print()


if __name__ == '__main__':
    mostrarDatos()
