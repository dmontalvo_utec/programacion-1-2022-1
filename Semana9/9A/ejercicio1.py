def generar_data():
    nombres_paises = ["Estados Unidos", "Unión Soviética", "Reino Unido",
                      "China", "Alemania"]
    medallas = [
        [1022, 794, 704], [395, 319, 296], [263, 295, 289],
        [227, 163, 153], [219, 246, 269]
    ]

    return nombres_paises, medallas


def mostrarMedallas(paises, medallas):
    print("%17s%7s%7s%7s" % (" ", "ORO", "PLATA", "BRONCE"))
    for i in range(len(paises)):
        print("%17s" % paises[i], end=" ")
        for j in range(len(medallas[i])):
            print("%6d" % medallas[i][j], end=" ")
        print()


def medallas_china(medallas):
    "Calcula e imprime el total de medallas obtenidas por China"
    total = 0
    for j in range(len(medallas[3])):
        total = total + medallas[3][j]
    print("\nEl total de medallas de China: ", total)


def medallas_bronce(medallas):
    "Calcula e imprime el total de medallas de bronce"
    total = 0
    for i in range(5):
        total = total + medallas[i][2]
    print("\nEl total de medallas de bronce: ", total)


def total_medallas(medallas):
    suma = 0
    for i in range(len(medallas)):
        for j in range(len(medallas[i])):
            suma = suma + medallas[i][j]
    print("\nLa cantidad total de medallas es: ", suma)


if __name__ == '__main__':
    paises, medallas = generar_data()
    mostrarMedallas(paises, medallas)

    medallas_china(medallas)
    medallas_bronce(medallas)
    total_medallas(medallas)
