TRIMESTRES = 3
DIVISIONES = 5

divisiones = ["Línea Blanca", "Electrodomésticos", "Juguetería",
              "Perecibles", "Limpieza"]
datos = [[450, 650, 342], [340, 487, 767], [134, 212, 354],
         [180, 464, 565], [647, 324, 232]]


def mostrar_matriz():
    print("DIVISION          Trimestre 1 Trimestre 2 Trimestre 3")
    for i in range(DIVISIONES):
        print(divisiones[i].ljust(18), end="")
        for j in range(TRIMESTRES):
            print(str(datos[i][j]).ljust(12), end="")
        print()


def calculo_total_jugueteria():
    suma = 0
    i = 2  # jugueteria
    for j in range(TRIMESTRES):
        suma = suma + datos[i][j]
    print("\nEl total de ventas de la División Juguetería es: ",
          suma)


def calculo_total_trimestre(trimestre):
    suma = 0
    j = trimestre
    for i in range(DIVISIONES):
        suma = suma + datos[i][j]
    print("\nEl total de ventas del Trimestre", trimestre, "es:", suma)


if __name__ == '__main__':
    mostrar_matriz()
    calculo_total_jugueteria()
    calculo_total_trimestre(1)
