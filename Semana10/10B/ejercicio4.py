"""
Dada una cadena de ADN generar su diccionario que tenga el primer y último elemento de la cadena
 indique cuantas veces aparece la primera y última base de la cadena.

 input: cccaactgaa
 output: c:4 a:4

 input: gtactcgggt
 output: {'g': 4, 't': 3}
"""


def contar(cadena: str):
    conteo = dict()
    conteo[cadena[0]] = 0
    conteo[cadena[-1]] = 0
    for letra in cadena:
        if letra in conteo:
            conteo[letra] += 1
    return conteo


if __name__ == '__main__':
    print("Ingrese una cadena de AN para contar el 1er y último elemento.")
    print("Espacio en blanco para finalizar el programa.")
    while True:
        palabra = input("Cadena 'ADN' : ")
        if palabra == '':
            break
        print(contar(palabra))
