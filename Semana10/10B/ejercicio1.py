"""
Haciendo uso de un diccionario, implemente un algoritmo que permita determinar cuántas veces se repite cada carácter de un string.
Considere que su algoritmo debe considerar cualquier caracter (letras, números, símbolos, etc.) excepto espacios en blanco.


input  :    aaabbaa
output :    a: 5, b: 2

"""


def contar_letras(palabra):
    letras = dict()
    for letra in palabra:
        if letra in letras:
            letras[letra] += 1
        else:
            letras[letra] = 1
    return letras


if __name__ == '__main__':
    print("Ingrese una palabra para contar las letras.\nEspacio en blanco para finalizar.")
    while True:
        palabra = input("Palabra : ")
        if palabra == '':
            break
        print(contar_letras(palabra))
