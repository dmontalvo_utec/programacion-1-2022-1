"""
Desarrollar un programa en Python que permita ingresar “N” DNIs a un diccionario
(se termina de ingresar con un DNI = -1).
Al terminar debe mostrar el DNI con el máximo valor.
Validar que el DNI debe tener 8 dígitos,
sino mostrar el mensaje DNI INCORRECTO.
Recordar cada DNI ingresado debe ser único.

Dni 1 : 99988877
Dni 2: 654877
DNI INCORRECTO
DNI 2: 11122233
Dni 3: -1

El DNI con máximo valor es 9998877

Comparacion de cadenas? O suma de digitos
"""


def leer_dnis():
    """
    Retornar un diccionario con los DNI y su valor
    :return:
    """
    pass


def maximo_dni(dnis):
    pass


if __name__ == '__main__':
    dic_dnis = leer_dnis()
    print("El DNI con maximo valor es", maximo_dni(dic_dnis))
