def inicializar(d):
    d['001'] = ["Pancho Perez", 'pperez@utec.edu.pe', "CS", [15, 20, 18]]
    d['002'] = ["Carla Costa", 'ccosta@utec.edu.pe', "Industrial", [18, 19, 18]]
    d['003'] = ["Sarita Sanchez", 'ssanchez@utec.edu.pe', "Bioingeniería", [15, 12, 16]]


def escribir_diccionario(valor: dict):
    for item in valor.items():
        print(item)


if __name__ == '__main__':
    # Creando y agregando
    print("Creando diccionario")
    alumnos = dict()
    alumnos['001'] = ["Pancho Perez", 'pperez@utec.edu.pe', "CS", [15, 20, 18]]
    alumnos['002'] = ["Carla Costa", 'ccosta@utec.edu.pe', "Industrial", [18, 19, 18]]
    alumnos['003'] = ["Sarita Sanchez", 'ssanchez@utec.edu.pe', "Bioingeniería", [15, 12, 16]]
    alumnos['004'] = ["Marco Medina", 'mmedina@utec.edu.pe', "Ciencia de Datos", [12, 13, 14]]

    print(alumnos)

    # Accediendo a los datos
    print(alumnos.keys())
    print(alumnos.values())
    print(alumnos.items())

    escribir_diccionario(alumnos)

    # Modificando elementos
    print("\nModificando elementos")
    alumnos['003'] = ["Sarita Sanchez", 'ssanchez@utec.edu.pe', "Bioingeniería", [20, 19, 19]]
    escribir_diccionario(alumnos)

    # Asignando y copiando diccionarios
    print("\nCopiando diccionarios")
    asesores = alumnos
    asesores['005'] = "Asesor"
    escribir_diccionario(alumnos)

    # Eliminando elementos
    print("\nEliminando elementos")

    del alumnos['004']
    escribir_diccionario(alumnos)

    alumnos.clear()
    escribir_diccionario(alumnos)

    inicializar(alumnos)
    del alumnos
    try:
        escribir_diccionario(alumnos)
    except Exception as e:
        print("Ocurrio un error.", e)
