"""
Diseñe e implemente un algoritmo que reciba como dato de entrada un texto
y proceda a extraer todas las palabras del texto asociando la cantidad de veces que se repite dicha palabra en el texto.

Use un diccionario para guardar cada palabra diferente y las veces que se repite.

Ej.
Cuando cuentes cuentos cuenta cuantos cuentos cuentas porque si cuentas cuantos cuentos cuentas sabras cuantos cuentos sabes contar

"""


def contar_palabras(frase: str):
    palabras = dict()
    for palabra in frase.split(" "):
        pa = palabra.lower()
        if pa in palabras:
            palabras[pa] += 1
        else:
            palabras[pa] = 1
    return palabras


if __name__ == '__main__':
    print("Ingrese una frase para contar las palabras.\nEspacio en blanco para finalizar el programa.")
    while True:
        palabra = input("Frase : ")
        if palabra == '':
            break
        print(contar_palabras(palabra))
