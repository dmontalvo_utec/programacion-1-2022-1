"""
Use un diccionario para crear un traductor de numeros del 1 al 10 al japonés. Tomar en cuenta la siguiente equivalencia.

1,2,3,4,5,6,7,8,9,10 = Ichi, Ni, San, Yon, Go, Roku, nana, Hachi, Kyu, Ju

"""

numeros = {
    1: "Ichi",
    2: "Ni",
    3: "San",
    4: "Yon",
    5: "Go",
    6: "Roku",
    7: "nana",
    8: "Hachi",
    9: "Kyu",
    10: "Ju"
}


def traducir(dato):
    return numeros[dato]


if __name__ == '__main__':
    print("Ingrese un número del 1 al 10")
    while True:
        dato = int(input("Dato[1-10] :"))
        if dato < 1 or dato > 10:
            break
        print(f"{dato} en japonés es {traducir(dato)}")
