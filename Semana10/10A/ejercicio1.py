"""
Escribir un programa que genere la lista de números entre 0 y 50
que no sean pares y que no sean múltiplos de 3.
"""
if __name__ == '__main__':
    print([a for a in range(0, 51) if (a % 2 != 0 or a % 3 != 0)])
