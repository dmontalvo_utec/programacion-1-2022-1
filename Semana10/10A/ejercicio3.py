"""
Dado el siguiente texto:

texto = "10, 20, 33, 40, 11, 90"

Escribir un programa que genere una lista con 6 valores numéricos
incluidos en el texto y luego calcular la suma de aquellos valores
múltiplos de 10.

"""

if __name__ == '__main__':
    texto = "10, 20, 33, 40, 11, 90"
    enteros = [int(a) for a in texto.split(",")]
    print(enteros)
    multiplos = [e for e in enteros if e % 10 == 0]
    print(multiplos)
    suma = sum(multiplos)
    print("Suma de Multiplos de 10 =", suma)

    print("Suma de multiplos de 10 =", sum([e for e in [int(a) for a in texto.split(",")] if e % 10 == 0]))
