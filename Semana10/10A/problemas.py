def problema1():
    """
    A = {2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24}
    A = { x | x ∈ N, x es par, x ≥ 2 ∧ x ≤ 24}
    :return:
    """
    lista = []
    for i in range(2, 25, 2):
        lista.append(i)
    print(lista)

    lista2 = [a for a in range(2, 25, 2)]
    print(lista2)

    print([a ** 2 for a in range(100)])


def problema2():
    """
    B = {3, 6, 9, 12, 15, 18, 21, 24, 27}
    B = { x | x ∈ N, x es múltiplo de 3, x ≥ 3 ∧ x ≤ 27}

    :return:
    """
    print([a for a in range(3, 28, 3)])
    print([3 * a for a in range(1, 10)])
    print([a for a in range(1, 28) if a % 3 == 0])


def problema3():
    """
    (A ∩ B) = { x | x ∈ N, x es par, x es múltiplo de 3, x ≥ 3 ∧ x ≤ 24}
    (A ∩ B) = { x | x ∈ A Λ x ∈ B }
    :return:
    """
    A = [a for a in range(2, 25, 2)]
    B = [a for a in range(3, 28, 3)]
    print([x for x in A + B if (x in A and x in B)])


if __name__ == '__main__':
    print("Problema 1")
    problema1()
    print("Problema 2")
    problema2()
    print("Problema 3")
    problema3()
