"""
Escribir un programa que genere la lista de 20 números aleatorios
entre 1 y 15.

"""
import random

if __name__ == '__main__':
    print([random.randint(1, 15) for a in range(20)])
