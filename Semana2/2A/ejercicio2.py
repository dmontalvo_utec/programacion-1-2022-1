'''
Desarrolle un programa que permita hallar la distancia entre dos puntos.
Puede utilizar la siguiente fórmula para calcular la distancia:

distancia = raiz (  (x2-x1)^2 + (y2-y1)^2 )
'''
import math

x1, y1 = input("Ingrese la coordenada 1 (x,y) :").split(sep=",")
# Ojo a esta forma de leer. Tiene que ingresar 1,5 por ejemplo

x1 = int(x1)
y1 = int(y1)

print("Ingrese la coordenada 2")
# Aqui si ingrese X y Y separados
x2 = int(input("X2 : "))
y2 = int(input("Y2 : "))

distancia = math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)

print(f"La distancia entre los puntos ({x1},{y1}) y ({x2},{y2}) es {distancia}")
