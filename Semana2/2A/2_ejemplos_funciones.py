import math  # MODULO MATH

print("Funciones matemáticas")
print(f"Abs(-5) : {abs(-5)}")
print(f"Round(3.52) : {round(3.52)}")
print(f"Round(PI,2) : {round(math.pi, 2)}")

print(math.sqrt(25.0))
print(math.trunc(3.52))
print(math.cos(0.5))
print(math.sin(0.5))
print(math.tan(1.5))
print(math.exp(2))
print(math.degrees(2.5))
print(math.radians(2.5))
print(math.log(100))
print(math.log(100, 2))
