'''
Desarrolle un programa que permita convertir segundos a: días, horas, minutos y segundos.
El programa deberá mostrar el equivalente de los segundos utilizando el formato:
D:HH:MM:SS, donde D,HH,MM y SS representan los días, horas, minutos y segundos respectivamente.
Las horas, minutos y segundos deberá formatearse de tal manera que solo ocupe exactamente dos dígitos,
incluyendo el 0 si es necesario.

'''

segundos = int(input("Ingrese un número de segundos : "))

dias = segundos // (60 * 60 * 24)
aux = segundos % (60 * 60 * 24)
horas = aux // (60 * 60)
aux = aux % (60 * 60)
mins = aux // 60
seg = aux % 60

dias = '{:0>2}'.format(dias)
horas = '{:0>2}'.format(horas)
mins = '{:0>2}'.format(mins)
seg = '{:0>2}'.format(seg)

print(f"{segundos} segundos equivalen a {dias}:{horas}:{mins}:{seg} (D:HH:MM:SS)")
