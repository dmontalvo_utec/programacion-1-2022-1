# operadores aritmeticos
print("**** OPERADORES ARITMETICOS ****")

print("Suma")
x = 23 + 67.78
print(x)

print("Resta")
w = 45.67 - 20
print(2)

print("Multiplicación")
z = 34 * 2.5
print(z)

print("División real")
f = 7 / 2
print(f)

print("División entera")
a = 7 / 2
print(a)

print("Residuo (Módulo)")
res = 7 % 2.3
print(res)

res2 = 7 % 9
print(res2)

print("Potencia")
pow = 2 ** 8
print(pow)

pow2 = 16 ** 0.5
print(pow2)
