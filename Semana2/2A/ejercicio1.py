'''
Desarrolle un programa que permita leer el peso de una persona expresado en gramos,
la altura expresada en centímetros y el programa calcule el Índice de masa corporal.

El índice de la masa corporal, de las siglas en inglés BMI  se calcula utilizando la siguiente fórmula:


BMI = peso(kg)  /  altura(m)*altura(m)

'''

gramos = int(input("Ingrese su peso en gramos : "))
altura = float(input(("Ingrese su altura en cms : ")))

bmi = (gramos / 1000.0) / (altura / 100) ** 2

print(f"Su indice de masa corporal BMI es {bmi}")
