'''
Realice un programa que permita leer un número entero de 3 dígitos y el programa imprima el número al revés.

Desarrolle el programa utilizando los operadores matemáticos y trabaje bajo el supuesto
que el usuario siempre ingresará un número de 3 dígitos.

Recuerda las Unidades, Decenas, Centanas, Unidades de Mil?

'''

num = int(input("Ingrese número de 3 digitos : "))

cen = num % 10
aux = num // 10
dec = aux % 10
unid = aux // 10

nuevo_num = unid + dec * 10 + cen * 100

print(f"El número ingresdo {num} invertido es igual a {nuevo_num}")
