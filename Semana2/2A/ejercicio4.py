'''
Tai Loy, ha decidido empaquetar en sus bodegas colores, en cajas de 6, 12 y 24 unidades.
De tal manera que reducirá sus costos al ser ellos mismos quienes realicen el empaque de colores.

Se pide realizar un programa que capture como dato de entrada un número entero que representaría
la cantidad de colores a ser empaquetados y el programa halle el menor número de cajas de colores de 24, 12 , 6
e indicar el número de colores sobrantes
'''

nro_colores = int(input("Ingrese el nro de colores a empaquetar : "))

cajas_24 = nro_colores // 24
restante = nro_colores % 24
cajas_12 = restante // 12
restante = restante % 12
cajas_6 = restante // 6
restante = restante % 6

print(f"Ingresó {nro_colores} colores.")
print(f"Se dividirán en {cajas_24} cajas de 24, {cajas_12} cajas de 12 y {cajas_6} cajas de 6.")
print(f"Sobran {restante} colores")
