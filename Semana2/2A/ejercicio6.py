'''
Un polígono regular, es aquel que tiene longitud de sus lados iguales y el ángulo entre los lados adyacentes es igual.
Desarrolle un programa que permita calcular el área de un polígono regular.

Puede calcular el área de un polígono regular utilizando la siguiente fórmula:

area = n * s^2 / 4 * tan(PI/n)

Donde:
s   es la longitud de los lados (Perímetro)
n   es el número de lados


'''
import math

n = int(input("Ingrese el nro de lados del polígono :"))
lado = float(input(f"Ingrese la longitud de cada lado de su polígono de {n} lados : "))

area = n * lado ** 2 / (4 * math.tan(math.pi / n))

print(f"El área de un polígono regular de {n} lados con longitud {lado} es igual a {area}")
