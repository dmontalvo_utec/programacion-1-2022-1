'''
Realice un programa que permita leer 3 números enteros y el programa luego los imprima en orden ascendente.

Utilice las funciones: min y max

'''

a = int(input("Ingrese A : "))
b = int(input("Ingrese B : "))
c = int(input("Ingrese C : "))

menor = min(a, b, c)
mayor = max(a, b, c)
medio = a + b + c - menor - mayor

print(f"{menor} < {medio} < {mayor}")
