'''
Escriba un código que calcule el consumo de electricidad en soles dado el consumo en kW, con las siguientes condiciones:

- si el consumo es menor o igual a 100 kW, el kW cuesta 0.4522 Soles
- si el consumo es mayor que 100 kW, se aplica la tarifa anterior hasta 100 kW
  y 0.7 Soles por kW para el consumo sobre 100 kW

Realice el programa sin utilizar estructuras de control

'''

consumo = int(input("Ingrese nro de kw : "))

mayor_100 = consumo > 100

costo_menor_100 = consumo * 0.4522
costo_mayor_100 = 100 * 0.4522 + (consumo - 100) * 0.7

costo_total = (not mayor_100) * costo_menor_100 + mayor_100 * costo_mayor_100

print(f"Costo total = {costo_total}")
