'''
Desarrolle un programa que permita leer un número entero y el programa indique si el número es par o impar.
Realice el programa sin utilizar estructuras de control.

'''

num = int(input("Ingrese un número entero : "))

par = num % 2 == 0

print(f"El numero {num}", "es" * par, "no es" * (not par), "par")
