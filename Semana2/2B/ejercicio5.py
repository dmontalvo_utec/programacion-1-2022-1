'''
La municipalidad de Miraflores, ha decidido incentivar el reciclaje de botellas entre los vecinos de este distrito.
Así por cada botella que se entregue al municipio devolverá dinero.

Por cada botella de hasta un litro devolverá 1.25 sol y por cada botella cuya capacidad sea de más de un litro
devolverá 3.75 soles.

Realice un programa que permita calcular el monto a devolver al vecino por las botellas que entregue al municipio.

Imprima el resultado con dos decimales.

'''

botellas = int(input("Ingrese número de botellas de HASTA 1L : "))
botellas1L = int(input("Ingrese número de botellas de MÁS de 1L : "))

devolucion = botellas * 1.25 + botellas1L * 3.75

print(f"Recibirá una devolución de S/{round(devolucion, 2)}")
