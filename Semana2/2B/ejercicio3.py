'''
Sean s1, s2 y s3 las longitudes de los tres lados de un triángulo, para verificar si esos tres lados
realmente forman un triángulo debe de cumplirse la propiedad de que la suma de 2 de los lados sea mayor al otro lado,
es así por ejemplo que la suma

s1 + s2 > s3,  s2 + s3 > s1 y  s1 + s3 > s2,

si se cumplen estas 3 ecuaciones podemos decir que los 3 lados forman un triángulo.

Desarrollar un programa que lea la longitud de los lados de un triángulo y que muestre "ES TRIANGULO VALIDO"
si se cumple la regla mencionada arriba o que muestre "NO ES TRIANGULO VALIDO" si no se cumple la regla.

Realice el programa sin utilizar estructuras de control

'''

s1, s2, s3 = map(float, input("Ingrese los 3 lados de un triangulo, separados or coma (,) : ").split(","))

# otra forma (sin map)
# s1, s2, s3 = input("Ingrese los 3 lados de un triangulo, separados or coma (,) : ").split(",")
# s1, s2, s3 = float(s1), float(s2), float(s3)

es_triangulo = s1 + s2 > s3 and s2 + s3 > s1 and s1 + s3 > s2

print((not es_triangulo) * "NO", "ES TRIANGULO VÁLIDO")
