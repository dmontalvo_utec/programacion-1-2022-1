'''
El dueño de un restaurante, ha decidido incluir un porcentaje del 5% por la atención de los mozos como concepto
de propina e incluir el impuesto del IGV de 18 %, al realizar el cálculo del monto a pagar por sus clientes.
El impuesto del IGV y el porcentaje por el concepto de las propinas se aplican sobre el monto consumido.

Con esta información, se solicita se realice un programa que permita calcular el monto a pagar por el cliente.

randint -> aleatorio entre 1 y 1000
ljust -> left justified. Texto alineado a la izquierda
rjust -> right justified. Texto alineado a la derecha
center -> center. Texto centrado en un espacio de N caracteres

'''
import random

precio = float(input("Ingrese el valor de consumo : "))

nro_boleta = "{:05d}".format(random.randint(1, 1000))
propina = precio * 0.05
igv = precio * 0.18

print("La Bistecca".center(30))
print(f"*** Boleta Nro {nro_boleta} ***".center(30))
print(f"{'Consumo'.ljust(15)}: S/ {precio}")
print(f"{'Propina(5%)'.ljust(15)}: S/ {propina}")
print(f"{'I.G.V.(18%)'.ljust(15)}: S/ {igv}")
print("-" * 30)
print(f"{'TOTAL'.ljust(15)}: S/ {precio + propina + igv}")
