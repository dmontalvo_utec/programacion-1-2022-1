# Calcule mentalmente las respuestas

print("-2 ** 2 >>", -2 ** 2)
print("4 > 4.0 >>", 4 > 4.0)
print("5 + 3 / 2 * 4 >>", 5 + 3 / 2 * 4)
print("5 + 1 != 3 * 10 >>", 5 + 1 != 3 * 10)
print("5 + (1 != 3) * 10 >>", 5 + (1 != 3) * 10)
print("(1 == 4) * 50 >>", (1 == 4) * 50)
print("(10 < 4) or (7 > 9) and (5 > 4.5) >>", (10 < 4) or (7 > 9) and (5 > 4.5))
print("(5 > 4.5) or (7 > 9 or 10 < 4) >>", (5 > 4.5) or (7 > 9 or 10 < 4))

print("False^False,False^True >> ", True ^ 1==1, False ^ True)

