"""
Realice un programa que permita leer un número entero de 4 dígitos y el programa imprima y se visualice
la suma de los dígitos tal y como se muestra en el ejemplo.

Trabaje bajo el supuesto, que el usuario siempre ingresará un número entero de 4 dígitos.

Numero : 6521
6+5+2+1=14

Numero : 8912
8+9+1+2=20
"""

num = int(input("Ingrese un número entero de 4 dígitos : "))

u = num % 10
aux = num // 10
d = aux % 10
aux = aux // 10
c = aux % 10
um = aux // 10

print(f"Número : {num}\n{um}+{c}+{d}+{u}={um + c + d + u}")
