"""
Realice un programa que lea un dato un número mayor a 3 que será la altura del triangulo
Luego lea un dato 'limite' que tenga un valor entre 2 y 9 (validelo)
que será el máximo numero de digitos a utilizar para construir la piramide.

Los digitos a utilizar seran desde 1 hasta el dato ingresado en limite.
Los digitos se incrementan sin importar el salto de linea.

El programa imprimirá un triángulo como se muestra en el ejemplo.

filas: 2
filas: 3
limite: 1
limite: 2

          1
         212
        12121

filas: 5
limite: 9
          1
         234
        56789
       1234567
      891234567

"""
