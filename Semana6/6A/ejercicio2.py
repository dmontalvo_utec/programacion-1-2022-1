"""
Realice un programa que sume los números pares desde el 2 al 100.
"""

suma = 0
for i in range(2, 101, 2):
    suma += i
print(suma)

print(sum(list(range(2, 101, 2))))
