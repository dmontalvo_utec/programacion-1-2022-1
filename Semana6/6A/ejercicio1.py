"""
Imprima 7 veces la palabra Rio.
Imprima esta secuencia de numeros: 0,-1,-2,-3,-4,-5,-6,-7, -8
Imprima esta secuencia de números:  90, 85, 80, 75, 70 ....25
"""

for i in range(7):
    print("Rio", end=",")
print()
for i in range(0, -9, -1):
    print(i, end=",")
print()
for i in range(90, 24, -5):
    print(i, end=",")
