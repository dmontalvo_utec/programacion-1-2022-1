"""
Realice un programa que lea como dato un número mayor a cero y el programa imprima un triángulo como se muestra en el ejemplo.

filas: 8

        #
       ##
      ###
     ####
    #####
   ######
  #######
 ########
"""

while True:
    dato = int(input("Dato : "))
    if dato > 0:
        break

for i in range(dato):
    for j in range(dato - i):
        print(" ", end="")
    for k in range(i):
        print("#", end="")
    print()

