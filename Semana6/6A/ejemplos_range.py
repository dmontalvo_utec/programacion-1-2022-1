print("\n--Range con 1 parametro--")
for numero in range(7):
    print(numero)

print("\n--Range con 1 parametro + Texto--")
for i in range(5):
    print(i, "UTEC")

print("\n--Range con 2 parametros--")
for i in range(3, 8):
    print("El valor de i es: ", i)

print("\n--Range con 3 parametros--")
for i in range(1, 6, 2):
    print("El valor de i es: ", i)

print("\n--Range con 3 parametros con retroceso--")
for i in range(6, 1, -1):
    print("El valor de i es: ", i)
