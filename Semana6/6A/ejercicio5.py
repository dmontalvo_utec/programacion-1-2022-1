"""
Escribir un código en Python que reciba una frase e imprima el número de letras que la frase contiene.
Por ejemplo: “hola”, tiene 4 letras.

No use la función len.

"""

frase = input("Frase : ")

cont = 0
for letra in frase:
    if letra != ' ':
        cont += 1

print(f"La frase '{frase}' tiene {cont} letras")
