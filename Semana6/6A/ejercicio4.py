"""
Realice un programa que lea como dato un número mayor a cero y el programa imprima un triángulo como se muestra en el ejemplo.

filas: 7
          1
         123
        12345
       1234567
      123456789
     12345678901
    1234567890123
"""

dato = int(input("Dato : "))
while dato <= 0:
    dato = int(input("Dato : "))

i = 1
for i in range(dato):
    len = 2 * i + 1
    numeros = ''
    for j in range(1, len + 1):
        numeros += str(j % 10)
        # print(j % 10, end='')
    print(numeros.center(dato * 2 - 1))
    # print()