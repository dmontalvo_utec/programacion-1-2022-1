"""
Escribir un programa que permita calcular el valor de una función compuesta.
Sea f (x) = 3x + 5
y g(x) = x2, hallar f (g(x)).

"""


def leerX():
    x = int(input("Ingrese un valor para x: "))
    return x


def f(x):
    return 3 * x + 5


def g(x):
    return x ** 2


if __name__ == '__main__':
    x = leerX()
    valor = f(g(x))
    print(f"El valor de la función compuesta es {valor}")
