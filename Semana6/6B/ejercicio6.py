"""
El director de la universidad debe escoger a k estudiantes de un total de N
para representar a la institución en un evento internacional.
 ¿Entre cuántos grupos de k alumnos distintos puede elegir?

Ejemplos:
N: 10
k: 6
Grupos distintos: 210

N: 20
k: 8
Grupos distintos: 125970

Sabiendo que el problema se resuelve aplicando combinatoria.
Diseñe una función comb(N, k) que aplique la fórmula de
la combinatoria y dentro de ella llame a la función fact(n) para calcular el factorial de n.

"""


def fact(n):
    factorial = 1
    while n >= 1:
        factorial *= n
        n -= 1
    return factorial


def comb(n, k):
    return fact(n) // (fact(k) * fact(n - k))


if __name__ == '__main__':
    N = int(input("N : "))
    k = int(input("k : "))
    print(f"Grupos distintos : {comb(N, k)}")
