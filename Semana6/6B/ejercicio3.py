"""
Elabore un algoritmo e implemente una función que permita hallar la hipotenusa de un triángulo rectángulo,
dados los valores de sus catetos.
"""
import math


def hipotenusa(c1, c2):
    return math.sqrt(c1 ** 2 + c2 ** 2)


cat1 = float(input("Primer cateto : "))
cat2 = float(input("Segundo cateto : "))
h = hipotenusa(cat1, cat2)

print("La hipotenusa es: ", h)
