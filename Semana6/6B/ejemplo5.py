"""
Que imprime el siguiente codigo?"
"""


def funcion(a, b: list):
    for i in range(len(a)):
        a[i] = a[i] * 2
    for j in range(len(b)):
        b[j] = b[j] * 3


x = [1, 2]
y = [3, 4, 5]
funcion(x, y)
print(x, y)
