"""
Funcion promedio
"""

def promedio(a, b, c):
    '''
    Calcula el promedio de 3 números a,b,c

    :param a: primer número
    :param b: segundo número
    :param c: tercer número
    :return: promedio de a,b y c
    '''
    suma = a + b + c
    prom = suma / 3
    return prom

n1 = int(input("Dato 1 : "))
n2 = int(input("Dato 2 : "))
n3 = int(input("Dato 3 : "))
avg = promedio(n1, n2, n3)
print(f"El promedio es {avg}")
