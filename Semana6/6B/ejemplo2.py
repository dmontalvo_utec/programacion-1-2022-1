"""
Cree una funcion para calcular el termino de la secuencia fibonacci

0,1,1,2,3,5,8,13,21,34,55,89 ...
a b a+b
  a b a+b
    a b

"""


def fibonacci(n):
    """
    Retorna el elemento n de la secuencia fibonacci

    :param n: Posición N
    :return: Elemento en la posición N
    """
    a, b = 0, 1
    for i in range(n):
        a, b = b, a + b
    return b

print(fibonacci(5))
