"""
Implemente 3 funciones para leer datos.

- leerEntero(mensaje,min,max)
- leerDecimal(mensaje,min,max)
- leerString(mensaje)

Las funciones leerEntero y leerDecimal recibirán como parametro el texto con el que solicitar la entrada
y el minimo y maximo valor (Incluyente) que deben aceptar. Si se ingresa un valor fuera de ese rango,
debe volver a solicitar otro dato
La funcion leerString debe recibir solo el mensaje que solicitará en la entrada.

Ej.

leerEntero("Dato entero",100,1000)

Salida

"Dato entero" :  10
"Dato entero" :  90
"Dato entero" :  99
"Dato entero" :  100

Fin de programa (Se retorno un dato entero con valor 100)

"""