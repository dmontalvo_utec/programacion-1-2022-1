"""
Escribir un programa que permita hallar la suma de los valores enteros de los caracteres de una palabra, según su posición en el alfabeto.

"""


def sumarLetras(palabra):
    suma = 0
    for letra in palabra.upper():
        sum += ord(letra) - ord("A") + 1
    return suma


palabra = input("Ingrese palabra : ")
print("Su valor es ", sumarLetras(palabra))
