"""
Desarrolle un programa que permita leer el radio de un círculo y que muestre su área.
Debe implementar la función CalcularAreaCirculo que recibirá como parámetros el radio y devolverá el área del círculo.

Ejemplos:

Radio: 15
El área es: 706.8583471

Radio: 21
El área es: 1385.44236

"""
import math


def CalcularAreaCirculo(radio):
    return math.pi * radio ** 2


if __name__ == '__main__':
    radio = float(input("Radio : "))
    area = CalcularAreaCirculo(radio)

    print(f"El área es : {area}")
