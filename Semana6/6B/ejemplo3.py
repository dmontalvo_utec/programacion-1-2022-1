"""
Paso de parámetros por valor y por referencia
Python: Siempre por referencia


"""


def promedio(a, b, c):
    '''
    Calcula el promedio de 3 números a,b,c
    '''
    print("Función: ", "id(a) =", id(a), "id(b) =", id(b), "id(c) =", id(c))
    suma = a + b + c
    prom = suma / 3
    print("Función: ", "id(prom) =", id(prom))
    return prom


if __name__ == '__main__':
    n1 = int(input("Dato 1 : "))
    n2 = int(input("Dato 2 : "))
    n3 = int(input("Dato 3 : "))

    print("Leídas variables n1,n2,n3")
    print("Main   : ", "id(n1)=", id(n1), "id(n2)=", id(n2), "id(n3)=", id(n3))

    avg = promedio(n1, n2, n3)
    print("Main   : ", "id(avg ) =", id(avg))
    print(f"El promedio es {avg}")
