"""
Escriba un programa que agregue “ing” al final de cualquier string (de longitud mínimo 3 caracteres).
Si el string ya termina en “ing” entonces agregar ly.
Si el string tiene menos de 3 caracteres, devuelve el mismo string.

Ejemplo:
input: “Runn” output: “”Running
input: “Dying” output: “Dyingly”

"""


def agreganding(palabra):
    if len(palabra) < 3:
        return palabra

    if palabra[-3:] == "ing":
        return palabra + "ly"
    else:
        return palabra + "ing"


if __name__ == '__main__':

    while True:
        palabra = input("Input : ")

        if palabra.upper() == 'END':
            break

        print(agreganding(palabra))
