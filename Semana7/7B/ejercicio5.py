"""
Escriba un programa que dado 2 strings, s1 y s2, de input.
Imprima un nuevo string insertando s2 en la mitad de s1.

Ejemplo

input: “Batman”, “Robin” , output: “BatRobinman”

"""


def insertar_medio(s1, s2):
    mid = len(s1) // 2
    return s1[0:mid] + s2 + s1[mid:]


if __name__ == '__main__':

    while True:
        s1 = input("Input S1: ")
        s2 = input("Input S2: ")

        if s1.upper() == 'END' or s2.upper() == 'END':
            break

        print(insertar_medio(s1, s2))
