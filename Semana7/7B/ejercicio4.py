"""
Escriba un programa que dado un string como input y una posición
cree dos substrings y luego las imprima.
Si la posición es mayor al largo del string no imprimir nada.

Ejemplo

input: “Universidad” , 2 output: “Un” “iversidad”

"""


def subtrings(palabra, posicion):
    if posicion <= len(palabra):
        print(palabra[0:posicion], end=" ")
        print(palabra[posicion:])


if __name__ == '__main__':

    while True:
        palabra = input("Input : ")
        posicion = int(input("Posicion : "))

        if palabra.upper() == 'END':
            break

        subtrings(palabra, posicion)
