"""
Escriba un programa que dado un string cree 2 nuevos strings.
El primero con las letras minúsculas y el segundo con las letras mayúsculas.
Finalmente imprima ambos.

input: “PYthon” , output: “thon”, “PY”

"""


def separando(palabra: str):
    minus = ""
    mayus = ""
    for p in palabra:
        if p.isupper():
            mayus += p
        else:
            minus += p
    print(minus, end=" ")
    print(mayus)


if __name__ == '__main__':

    while True:
        palabra = input("Input : ")

        if palabra.upper() == 'END':
            break

        separando(palabra)
