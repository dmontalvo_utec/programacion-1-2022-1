"""
Escriba un programa que tome los últimos 2 caracteres de un string que ingrese el usuario
 y cree un nuevo string que repita 4 veces esa secuencia de 2 caracteres.
 Si la longitud del string original es menor que 2, devolver “no cumple”.

Ejemplo

input: “la” output: “lalalala”
input: “y” output: “no cumple”

"""


def repitiendo2ultimos(palabra):
    if len(palabra) < 2:
        return "No cumple"

    return 4 * palabra[-2:]


if __name__ == '__main__':

    while True:
        palabra = input("Input : ")

        if palabra.upper() == 'END':
            break

        print(repitiendo2ultimos(palabra))
