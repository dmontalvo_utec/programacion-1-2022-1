"""
Escriba un programa que permita ingresar un texto y luego muestre el mismo
en el formato (Mayús-Minús-Mayus-Minus…..)

Ejemplo

input: “Universidad” output: “UnIvErSiDaD”

"""


def camelCase(palabra: str):
    nueva_palabra = ""
    for i in range(len(palabra)):
        if i % 2 == 0:
            nueva_palabra += palabra[i].upper()
        else:
            nueva_palabra += palabra[i].lower()
    return nueva_palabra


if __name__ == '__main__':

    while True:
        palabra = input("Input : ")

        if palabra.upper() == 'END':
            break

        print(camelCase(palabra))
