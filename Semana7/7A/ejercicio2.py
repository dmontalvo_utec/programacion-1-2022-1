"""
Cree un módulo figura.py, el cual implemente funciones para calcular el área de:
un círculo, un cuadrado y triángulo.

Posteriormente, invoque a cada una de estas funciones desde un archivo ejercicio2.py

"""

import figura

if __name__ == '__main__':
    print("Circulo de r=20", figura.areaCirculo(20))
    print("Cuadrado de l=15", figura.areaCuadrado(15))
    print("Rectangulo de b=10, h=5", figura.areaRectangulo(10, 5))
