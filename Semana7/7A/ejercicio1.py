"""
Implemente la función area_triangulo de tal manera que el siguiente código funcione correctamente:
"""

def area_triangulo(base=2, altura=1):
    area = base * altura / 2
    return area


print(area_triangulo())
print(area_triangulo(2))
print(area_triangulo(2,1))

#---- output
#  1.0
#  1.0
#  1.0
