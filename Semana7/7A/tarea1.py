"""
RECIBO

implemente una funcion imprimir recibo que reciba 3 parametros

monto, IGV y propina.

- El valor monto es obligatorio, pero si no envia IGV y propina, puede asignarles por defecto 18% y 10%
- Si envia valores negativos, asuma 0 (para cualquier valor)

Imprimir el recibo en el siguiente formato (Considerar un ancho de 30 caracteres)

==============================
Restaurant "La sazón UTECina"

   Recibo N° {aleatorio}

   Monto   :  S/ 100,00
   IGV     :  S/  18.00
   Propina :  S/  10.00

   TOTAL   :  S/ 128.00
==============================

"""