from math import pi


def areaCirculo(r):
    return pi * r ** 2


def areaCuadrado(lado):
    return lado ** 2


def areaRectangulo(base, altura):
    return base * altura
