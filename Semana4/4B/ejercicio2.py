"""
Diseñe e implemente un programa para hallar la suma de los n primeros n úmeros de la
siguiente serie:
1, 4, 9, 16, 25, 36, 49, 64, ...
"""

n = int(input())

sum = 0
while n > 0:
    sum += n ** 2
    n -= 1

print(f"suma : {sum}")
