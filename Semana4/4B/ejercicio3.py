"""
Un profesor desea contar con un programa que le permita saber los porcentajes de
alumnos de su sección que han nacido en cada una de las estaciones del año.

Para ello se pide que elabore un programa que:

• Lea como dato la cantidad de alumnos, dato que debe ser mayor a 5 y menor a 41.
El programa debe validar el ingreso de esta información.

• Permita a cada alumno ingresar el día y el mes en que nació. No es necesario validar
el día y mes, se trabajaría bajo la premisa que cada alumno ingresaría datos correctos.
Con esta información se realizará el cálculo de los porcentajes de nacimiento en cada
estación de año.

• Finalmente, el programa mostrará los porcentajes.

Se le recuerda que las estaciones estan marcadas de acuerdo a las fechas que se indican
en la siguiente tabla:

|  Estación |   Inicio     |    Fin        |
| Verano    | 21 Diciembre |  20 Marzo     |
| Otoño     | 21 Marzo     |  21 Junio     |
| Invierno  | 22 Junio     |  22 Setiembre |
| Primavera | 23 Setiembre |  20 Diciembre |

"""

while True:
    dato = int(input("Numero de alumnos [6 - 40 ] : "))
    if 6 <= dato <= 40:
        break

i = 1
verano = 0
otonho = 0
invierno = 0
primavera = 0
while i <= dato:
    print("Fecha de nacimiento")
    d = int(input("Día : "))
    m = int(input("Mes : "))
    if m == 1 or m == 2 or (m == 12 and d >= 21) or (m == 3 and d <= 20):
        verano += 1
    elif m == 4 or m == 5 or (m == 3 and d >= 21) or (m == 6 and d <= 21):
        otonho += 1
    elif m == 7 or m == 8 or (m == 6 and d >= 22) or (m == 9 and d <= 21):
        invierno += 1
    else:
        primavera += 1
    i += 1

print(f"verano : {round(verano * 100 / dato, 3)}")
print(f"otoño : {round(otonho * 100 / dato, 3)}")
print(f"invierno : {round(invierno * 100 / dato, 3)}")
print(f"primavera : {round(primavera * 100 / dato, 3)}")
