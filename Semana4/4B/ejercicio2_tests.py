import unittest
from subprocess import Popen, PIPE

tested_file = "ejercicio2.py"


class MyTestCase(unittest.TestCase):
    def test_5(self):
        p = Popen(["python", tested_file], stdin=PIPE, stdout=PIPE, encoding='utf8')
        out = p.communicate("5")

        resultado = out[0].strip()
        esperado = "suma : 55"
        self.assertTrue(resultado.endswith(esperado), "Expected {0} ends with {1}".format(resultado, esperado))

    def test_7(self):
        p = Popen(["python", tested_file], stdin=PIPE, stdout=PIPE, encoding='utf8')
        out = p.communicate("7")

        resultado = out[0].strip()
        esperado = "suma : 140"
        self.assertTrue(resultado.endswith(esperado), "Expected {0} ends with {1}".format(resultado, esperado))


if __name__ == '__main__':
    unittest.main()
