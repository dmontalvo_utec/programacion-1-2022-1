import unittest
from subprocess import Popen, PIPE

tested_file = "ejercicio1.py"


class MyTestCase(unittest.TestCase):
    def test_edad_37_generacion_x(self):
        p = Popen(["python", tested_file], stdin=PIPE, stdout=PIPE, encoding='utf8')
        out = p.communicate("23\n6\n1983\n14\n2\n2021")

        edad = out[0].strip().split('\n')[-2]
        generacion = out[0].strip().split('\n')[-1]
        self.assertTrue(edad.endswith("Su edad es : 37"), "Expected {0} ends with {1}".format(edad, "Su edad es :37"))
        self.assertEquals(generacion.strip(), "Su generación es la Generación X")

    def test_edad_38_generacion_x(self):
        p = Popen(["python", tested_file], stdin=PIPE, stdout=PIPE, encoding='utf8')
        out = p.communicate("15\n2\n1983\n15\n2\n2021")

        edad = out[0].strip().split('\n')[-2]
        generacion = out[0].strip().split('\n')[-1]
        self.assertTrue(edad.endswith("Su edad es : 38"), "Expected {0} ends with {1}".format(edad, "Su edad es :38"))
        self.assertEquals(generacion.strip(), "Su generación es la Generación X")

    def test_edad_37_generacion_x_2(self):
        p = Popen(["python", tested_file], stdin=PIPE, stdout=PIPE, encoding='utf8')
        out = p.communicate("23\n9\n1983\n15\n2\n2021")

        edad = out[0].strip().split('\n')[-2]
        generacion = out[0].strip().split('\n')[-1]
        self.assertTrue(edad.endswith("Su edad es : 37"), "Expected {0} ends with {1}".format(edad, "Su edad es :37"))
        self.assertEquals(generacion.strip(), "Su generación es la Generación X")

    def test_edad_14_generacion_z(self):
        p = Popen(["python", tested_file], stdin=PIPE, stdout=PIPE, encoding='utf8')
        out = p.communicate("27\n7\n2006\n25\n12\n2020")

        edad = out[0].strip().split('\n')[-2]
        generacion = out[0].strip().split('\n')[-1]
        self.assertTrue(edad.endswith("Su edad es : 14"), "Expected {0} ends with {1}".format(edad, "Su edad es :14"))
        self.assertEquals(generacion.strip(), "Su generación es la Generación Z")


if __name__ == '__main__':
    unittest.main()
