import unittest
from subprocess import Popen, PIPE

tested_file = "ejercicio3.py"


class MyTestCase(unittest.TestCase):
    def test_7_alumnos(self):
        p = Popen(["python", tested_file], stdin=PIPE, stdout=PIPE, encoding='utf8')
        message = [3, 44, 7, 27, 9, 28, 9, 29, 9, 2, 10, 30, 6, 28, 6, 27, 6]
        out = p.communicate("\n".join(list(map(str, message))))

        resultado = out[0].strip()
        esperado = "verano : 0.0\notoño : 0.0\ninvierno : 42.857\nprimavera : 57.143"
        self.assertTrue(resultado.endswith(esperado), "Expected {0} ends with {1}".format(resultado, esperado))


if __name__ == '__main__':
    unittest.main()
