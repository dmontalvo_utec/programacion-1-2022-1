"""
Tema: Condicionales

Desarrolle un programa que permita tener como datos:
• La fecha en que una persona nacióo, en el formato: día, mes año
• La fecha actual, en el formato: día, mes, año
Y el programa indique: la edad y a qué generación pertenece, considerando los datos de la siguiente tabla:

| 13 - 20  | Generación Z  |
| 21 - 35  | Generación Y  |
| 36 - 59  | Generación X  |
| 60 a mas | Generación Baby Boomers |

Si la edad de la persona es menor a 13, el programa debe imprimir ”Su generación aun no tiene nombre asignado”
"""
print("Fecha de nacimiento")
dia_nac = int(input("Día: "))
mes_nac = int(input("Mes: "))
anio_nac = int(input("Año: "))

print("Fecha de actual")
dia = int(input("Día: "))
mes = int(input("Mes: "))
anio = int(input("Año: "))

if mes > mes_nac:
    edad = anio - anio_nac
else:
    if mes == mes_nac and dia >= dia_nac:
        edad = anio - anio_nac
    else:
        edad = anio - anio_nac - 1

print("Su edad es :", edad)

if edad < 13:
    print("Su generación no tiene nombre asignado")
elif 13 <= edad <= 20:
    generacion = "Generación Z"
elif 21 <= edad <= 35:
    generacion = "Generación Y"
elif 36 <= edad <= 59:
    generacion = "Generación X"
else:
    generacion = "Generación Baby Boomers"

print(f"Su generación es la {generacion}")
