"""
Una veterinaria le ha solicitado crear un programa para calcular la edad aproximada humana de sus pacientes caninos.
El programa que usted realizará solicita un número N que indica cuántos pacientes se atenderán.
A continuación solicita la edad canina y el nombre de cada paciente.
Por cada lectura, usted imprime el nombre y la edad real aproximada humana:

Considere que la edad real aproximada se calcula con los siguientes criterios.

 Los 2 primeros años se consideran como 10.5 años humanos cada uno.
 Cada año adicional se considera como 4 años humanos.
 Solo se considera edades en números enteros mayor o igual a 1.

Se solicita que este dato sea validado por su progra.
Es decir si no ingresa un valor mayor o igual a 1, debe volver a solicitar el dato.
El texto de salida debe combinar el nombre y la edad equivalente,
tal como se verá en los ejemplos de entrada y salida del programa, que se muestran en la diapositiva siguiente.

Ejemplo 2:

Numero de perritos : 2

Nombre del perro : Sultan
Edad canina : 10
La edad de  Sultan  es de  53

Nombre del perro : Argos
Edad canina : 1
La edad de  Argos  es de  10.5

"""
n = int(input("Número de perritos : "))

i = 1

while i <= n:
    nombre = input("Nombre del perro : ")
    edad = int(input("Edad canina : "))
    while edad < 1:
        edad = int(input("Edad canina : "))

    edad_humana = 0
    if edad <= 2:
        edad_humana = edad * 10.5
    else:
        edad_humana = 2 * 10.5 + (edad - 2) * 4
    print(f"La edad de {nombre} es {edad_humana}")
    i += 1
