"""
Realice un programa, que permita leer un número entero mayor que uno y determine si el número es un número perfecto o no.
Un número es Perfecto, cuando el número es igual a la suma de sus divisores positivos menores que él.
El 6 es un número perfecto porque la suma de sus divisores:  1 + 2 + 3 es igual a 6.
El 28 es un número Perfecto, porque la suma de sus divisores:  1 + 2 + 4 + 7 + 14 es igual a 28
Ejemplo 1:

Numero [ mayor a 1] : 0
Numero [ mayor a 1] : -4
Numero [ mayor a 1] : 28
El numero es Perfecto


"""

while True:
    dato = int(input("Numero [mayor a 1] : "))
    if dato > 1:
        break

sum_divisores = 0
divisor = 1
while divisor < dato:
    if dato % divisor == 0:
        sum_divisores += divisor
    divisor += 1

if sum_divisores == dato:
    print("El número es perfecto")
else:
    print("El número no es perfecto")
