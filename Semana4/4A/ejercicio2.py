'''
Desarrolle un programa que permita hallar la sumatoria de la siguiente serie:

x(1 a n)S x^5  =  1^5 + 2^5 + 3^5 .... + n^5

La serie corresponde a los números naturales, en donde cada número es elevado a la quinta.
Se pide que realice un programa que lea como dato el número del término y
halle la suma de los términos de serie desde 1 hasta el número que se ingresó como dato.
'''

num = int(input("Ingrese N : "))

i = 1
sum = 0
while i <= num:
    sum += i ** 5
    i += 1

print(f"La sumatoria de la serie con N={num} es {sum}")
