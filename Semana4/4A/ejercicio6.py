"""
Realice un programa, que permita leer varios números enteros hasta que se introduzca el cero.

Luego el programa mostrará lo siguiente:
La cantidad de números leídos
La cantidad de números pares
La cantidad de números impares

El cero, no debe entrar en el conteo.

"""

num = int(input("Ingrese número: "))
leidos = 0
pares = 0
impares = 0
while num != 0:
    leidos += 1
    if num % 2 == 0:
        pares += 1
    else:
        impares += 1
    num = int(input("Ingrese número: "))

# WHILE  (TRUE, DATO!=0)

print("Cantidad de nros leidos", leidos)
print("Cantidad de nros pares", pares)
print("Cantidad de nros leidos", impares)
