"""
Desarrolle un programa que permita hallar el factorial de un número entero positivo mayor que cero.
El programa debe validar el ingreso del dato, de tal manera que solo acepte números mayores o iguales a 1.

Ejemplo 1:

Numero [mayor o igual a 1] : -3
Numero [mayor o igual a 1] : 0
Numero [mayor o igual a 1] : 5
El factorial es: 120


Ejemplo 2:

Numero [mayor o igual a 1] : 7
El factorial es: 5040

"""

dato = int(input("Numero [mayor o igual a 1] : "))
while dato <= 1:
    dato = int(input("Numero [mayor o igual a 1] : "))

# Calculando el factorial
fact = 1
while dato > 0:
    fact = fact * dato
    dato -= 1

print(f"El factorial es {fact}")
