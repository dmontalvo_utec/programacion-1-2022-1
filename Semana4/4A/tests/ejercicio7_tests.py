import unittest
from subprocess import Popen, PIPE

tested_file = "../ejercicio7.py"


class MyTestCase(unittest.TestCase):
    def test_6_perfecto_intentos(self):
        p = Popen(["python", tested_file], stdin=PIPE, stdout=PIPE, encoding='utf8')
        out = p.communicate("-1\n0\n1\n6")
        self.assertTrue(out[0].endswith("El número es perfecto\n"))

    def test_6_perfecto(self):
        p = Popen(["python", tested_file], stdin=PIPE, stdout=PIPE, encoding='utf8')
        out = p.communicate("6")
        self.assertTrue(out[0].endswith("El número es perfecto\n"))

    def test_28_perfecto(self):
        p = Popen(["python", tested_file], stdin=PIPE, stdout=PIPE, encoding='utf8')
        out = p.communicate("28")
        self.assertTrue(out[0].endswith("El número es perfecto\n"))

    def test_27_no_perfecto(self):
        p = Popen(["python", tested_file], stdin=PIPE, stdout=PIPE, encoding='utf8')
        out = p.communicate("27")
        self.assertTrue(out[0].endswith("El número no es perfecto\n"))

    def test_5_no_perfecto(self):
        p = Popen(["python", tested_file], stdin=PIPE, stdout=PIPE, encoding='utf8')
        out = p.communicate("5")
        self.assertTrue(out[0].endswith("El número no es perfecto\n"))


if __name__ == '__main__':
    unittest.main()
