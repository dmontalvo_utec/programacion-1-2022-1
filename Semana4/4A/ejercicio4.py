"""
Desarrolle un programa que lea como dato un número cuyo valor puede ser desde 1 hasta el 500
y el programa cuente números desde 1 hasta el número que se ingresa como dato, pero con las siguientes indicaciones:

Si el número es múltiplo de 4, debe mostrar el número y luego “Tic”
Si el número es múltiplo de 6, debe mostrar el número y luego “Tac”
Si el número es múltiplo de 4 y de 6, debe mostrar el número y luego “Tic Tac”
En cualquier otro caso, solo debe mostrar el número.

Ejemplo 1:

Numero [1 - 500] : 700
Numero [1 - 500] : 900
Numero [1 - 500] : 34
1
2
3
4  Tic
5
6  Tac
7
8  Tic
9
10
11
12  TicTac

"""
import time

while True:
    dato = int(input("Numero [1 - 500] :"))
    if 0 < dato <= 500:
        break

i = 1
while i <= dato:
    if i % 4 == 0 and i % 6 == 0:
        print(i, "TIC TAC")
    elif i % 6 == 0:
        print(i, "TAC")
    elif i % 4 == 0:
        print(i, "TIC")
    else:
        print(i)
    i += 1
    time.sleep(0.2)
