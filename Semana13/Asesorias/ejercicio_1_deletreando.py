"""
Desarrolle un programa que genere de manera aleatoria ”n” n´umeros cuyo rango esten
desde el 100 al 999 y el programa genere un archivo texto llamado ”numeros.txt”,
en donde grabe en cada l´ınea: el n´umero que se gener´o y al costado deletree el n´umero
con palabras.
Escribe aquí el enunciado de tu pregunta:
• n debe ser mayor a 1
• Se sugiere utilizar un diccionario para realizar la conversión de un dígito a su equivalente
en palabras.
"""
import random

diccionario = {
    0: 'cero',
    1: 'uno',
    2: 'dos',
    3: 'tres',
    4: 'cuatro',
    5: 'cinco',
    6: 'seis',
    7: 'siete',
    8: 'ocho',
    9: 'nueve'
}


def deletreando(lista, archivo):
    with open(archivo, "w") as file:
        for numero in lista:
            file.write(f"{numero} {deletrear(numero)}\n")


def deletrear(num):
    texto = ""
    for digito in digitos(num):
        texto += diccionario[digito] + " "

    return texto.strip()


def digitos(num):
    """
     :param num: 123
     :return:  [1,2,3]
    """
    if num < 10:
        return [num]
    else:
        return digitos(num // 10) + [num % 10]


def leer_n():
    n = int(input("Dato (>1) :"))
    while n <= 1:
        n = int(input("Dato (>1) :"))
    return n


def lista_aleatoria(n):
    """
    Lista aleatoria con números del 100 al 999
    :param n:
    :return:
    """
    return [random.randint(100, 999) for a in range(n)]


if __name__ == '__main__':
    n = leer_n()
    lista = lista_aleatoria(n)
    print(lista)
    print("Deletreando ... ")
    deletreando(lista, "numeros.txt")
