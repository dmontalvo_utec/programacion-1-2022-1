"""
Diseñe e implemente un algoritmo recursivo que permita calcular la suma de los elementos
impares de una lista de números.
Algunos ejemplos de diálogo de este programa serían:
"""
import random


# [ 1 , 2 , 3 , 4 , 5 ]
def suma(lista):
    if len(lista) == 1:
        return zero_if_even(lista[0])
    else:
        return zero_if_even(lista[0]) + suma(lista[1:])


def zero_if_even(num):
    """
    Zero si num es par
    """
    return 0 if num % 2 == 0 else num


def generar_lista(n):
    return [random.randint(0, n) for i in range(n)]


if __name__ == '__main__':
    while True:
        dato = int(input("Ingrese un número >= 5 :"))
        if dato < 5:
            break
        lista = generar_lista(dato)
        print(lista)
        print("Suma impares =", suma(lista))
