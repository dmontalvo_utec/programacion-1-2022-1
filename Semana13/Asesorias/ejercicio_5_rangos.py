"""
Escribir una función recursiva que reciba como input dos números y devuelva como
resultado la suma de todos los números desde el inicio del rango (número menor) hasta
el final del rango (número mayor).
"""


# m = 5, n=10
# 5 + 6 + 7 + 8 + 9 + 10
# TODO modificar para que funcione si m O n son menores
def suma_rangos(m, n):
    if m == n:
        return m
    else:
        return m + suma_rangos(m + 1, n)


if __name__ == '__main__':
    print("Suma de 1 a 5 -> ", suma_rangos(1, 5))
    print("Suma de 5 a 10 -> ", suma_rangos(5, 10))
    print("Suma de 100 a 200 -> ", suma_rangos(100, 200))
