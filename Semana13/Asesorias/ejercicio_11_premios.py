peliculas = [
    {'anio': 2010, 'nombre': 'Andre Geim y Konstant n Novosiolov'},
    {'anio': 2011, 'nombre': 'Perlmutter , Schmidt y Adam Riess'},
    {'anio': 2012, 'nombre': 'Serge Haroche y David Wineland'},
    {'anio': 2013, 'nombre': 'Peter Higgs y Francoi s Engl e r t'},
    {'anio': 2014, 'nombre': 'Akasaki , Hi r o shi Amano y Nakamura'},
    {'anio': 2015, 'nombre': 'Takaaki Kaj i ta y Arthur B. McDonald'},
    {'anio': 2016, 'nombre': 'Thouless , Haldane y Kosterlitz'},
    {'anio': 2017, 'nombre': 'Rainer Weiss , Barry Barish y Thorne'},
    {'anio': 2018, 'nombre': 'Donna Strickland, Mourou y Ashkin'},
    {'anio': 2019, 'nombre': 'James Peebles , Mayor y Queloz'}]


def buscar(peliculas, anio):
    if len(peliculas) == 1:
        if peliculas[0]['anio'] == anio:
            return peliculas[0]['nombre']
        else:
            return None

    mid = len(peliculas) // 2
    if peliculas[mid]['anio'] == anio:
        return peliculas[mid]['nombre']
    elif anio < peliculas[mid]['anio']:
        return buscar(peliculas[:mid], anio)
    else:
        return buscar(peliculas[mid:], anio)


if __name__ == '__main__':
    while True:
        dato = int(input("Ingrese a año [2010-2019] :"))
        if 2009 < dato < 2020:
            break

    print(f"Los ganadores del año {dato}", buscar(peliculas, dato))
