"""
Desarrolle un programa que lea como dato un número “n” mayor a 10
y luego genere de manera aleatoria una lista con “n” números cuyo rango de valores estén entre 1 y 99.

Luego permita al usuario leer el dato a buscar y a través del algoritmo de la búsqueda binaria
determine si el dato se encuentra o no en la lista.

Diseñe el programa utilizando funciones.

"""
import random as rn


def busquedaBinaria(lista, busqueda):
    if len(lista) == 1:
        return lista[0] == busqueda
    mid = len(lista) // 2
    if lista[mid] == busqueda:
        return True
    elif busqueda < lista[mid]:
        return busquedaBinaria(lista[:mid], busqueda)
    else:
        return busquedaBinaria(lista[mid:], busqueda)


def leerDatoMayor10():
    n = 0
    while n <= 10:
        n = int(input("Ingrese N>10 :"))
    return n


def leerDato_1_99():
    n = 0
    while n < 1 or n > 99:
        n = int(input("Ingrese N<0,99] :"))
    return n


if __name__ == '__main__':
    lista = [rn.randint(1, 99) for i in range(leerDatoMayor10())]
    print("Lista generada :", lista)
    lista = sorted(lista)  # condicion para busqueda binaria
    encontrado = busquedaBinaria(lista, leerDato_1_99())
    print("Encontrado" if encontrado else "No encontrado")
