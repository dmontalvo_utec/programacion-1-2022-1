"""
Desarrolle un programa que permita almacenar varios nombres en una lista.
No se sabe cuantos nombres son, pero el ingreso de datos termina cuando el usuario ingresa la palabra “fin”, la cual no se incluye en la lista.

Luego el programa permite al usuario leer como dato un nombre y el programa indicará si el nombre
se encuentra registrado en la lista utilizando un algoritmo de búsqueda lineal.

Si encuentra el nombre, indique en qué posición se encuentra. Si no se encuentra, retorne un mensaje adecuado.

"""


def busqueda_lineal(lista, nombre):
    for pos, valor in enumerate(lista):
        if valor == nombre:
            return pos
    return -1


if __name__ == '__main__':
    nombres = []
    nombre = input("Nombre? :")
    while nombre != "fin":
        nombres.append(nombre)
        nombre = input("Nombre? :")

    buscar = input("Nombre a buscar?")

    index = busqueda_lineal(nombres, buscar)
    print(f"{buscar}{' no ' if index == -1 else ' '}encontrado {'en pos ' + str(index) if index != -1 else ''}")
