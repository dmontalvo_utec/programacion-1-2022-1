"""
Considerando la información del Sistema Solar que se muestra a continuación, desarrolle un programa que permita:
Leer como dato el nombre de un planeta y muestre todos los datos de dicho planeta
Utilice búsqueda lineal para hallar dicho planeta

"""
lista=[
   { "planeta": "Mercurio", "diametro":4878, "anio": 88,"dia": 59 },
   { "planeta": "Venus", "diametro":12104 , "anio":225  ,"dia":5784  },
   { "planeta": "Tierra", "diametro":12760 , "anio":365  ,"dia":24  },
   { "planeta": "Marte", "diametro":6787 , "anio":687  ,"dia":24  },
   { "planeta": "Jupiter", "diametro":139822 , "anio":4343  ,"dia":10 },
   { "planeta": "Saturno", "diametro":120500 , "anio": 10767 ,"dia": 11},
   { "planeta": "Urano", "diametro":51120 , "anio":30660  ,"dia":18},
   { "planeta": "Neptuno", "diametro":49530 , "anio":60225  ,"dia":19}]


def buscar_planeta(planeta):
    for p in lista:
        if p['planeta'] == planeta:
            return p
    return None


def buscar_planeta_b(planetas, nombre):
    if len(planetas) == 1:
        if planetas[0]['planeta'] == nombre:
            return planetas[0]

    mid = len(planetas) // 2
    if planetas[mid]['planeta'] == nombre:
        return planetas[mid]
    elif nombre < planetas[mid]['planeta']:
        return buscar_planeta_b(planetas[:mid], nombre)
    else:
        return buscar_planeta_b(planetas[mid:], nombre)


if __name__ == '__main__':
    planeta = input("Ingrese planeta : ")
    datos = buscar_planeta(planeta)
    print("Lineal :", datos)

    lista = sorted(lista, key=lambda p: p['planeta'])
    # [print(p) for p in lista]
    datos = buscar_planeta_b(lista, planeta)
    print("Binaria :", datos)
