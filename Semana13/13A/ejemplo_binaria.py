import random

import matplotlib.pyplot as plt
import numpy as np


def busqueda_binaria(lista, e):
    """
    La lista tiene que estar ordanada
    :param lista:
    :param e:
    :return:
    """
    if len(lista) == 1:
        return lista[0] == e

    middle = len(lista) // 2
    if lista[middle] == e:
        return True
    elif e < lista[middle]:
        return busqueda_binaria(lista[:middle], e)
    else:
        return busqueda_binaria(lista[middle:], e)


def busqueda_binaria_n(lista, e, n):
    """
    La lista tiene que estar ordanada
    :param lista:
    :param e:
    :return:
    """
    if len(lista) == 1:
        return lista[0] == e, n + 1

    middle = len(lista) // 2
    if lista[middle] == e:
        return True, n + 1
    elif e < lista[middle]:
        return busqueda_binaria_n(lista[:middle], e, n + 1)
    else:
        return busqueda_binaria_n(lista[middle:], e, n + 1)


def generate_list(n):
    return sorted([random.randint(0, n) for i in range(n)])


if __name__ == '__main__':
    valor = random.randint(1, 20)
    lista = generate_list(20)
    print(f"Buscando {valor} en {lista}->", busqueda_binaria(lista, valor))

    valor = random.choice(lista)
    encontrado, veces = busqueda_binaria_n(lista, valor, 0)
    print(f"Buscando {valor} en {lista}->", encontrado, f"{veces} busquedas")

    results = np.zeros((2000, 3), np.int16)
    for i in range(1, 2000):
        lista = generate_list(i)
        e = random.randint(0, i)  # dato a buscar
        results[i][0] = i  # tamaño lista
        results[i][1], results[i, 2] = busqueda_binaria_n(lista, e, 0)

    plt.plot(results[:, 0:1], results[:, 2:3], 'go')
    plt.ylabel("Intentos")
    plt.xlabel("Tamaño lista")

    plt.show()
    print("Finalizado")
