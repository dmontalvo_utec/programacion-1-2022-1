"""
Considerando la información, desarrolle un programa que permita leer como dato el año
 y muestre el título de la película, utilice el algoritmo de la búsqueda binaria.
"""


def busqueda_binaria(lista, busqueda):
    if len(peliculas) == 1:
        return peliculas[0] if peliculas[0]['año'] == busqueda else None

    middle = len(peliculas) // 2
    if peliculas[middle]['año'] == busqueda:
        return peliculas[middle]
    elif busqueda < peliculas[middle]['año']:
        return busqueda_binaria(peliculas[:middle], a)
    else:
        return busqueda_binaria(peliculas[middle + 1:], a)


if __name__ == '__main__':
    peliculas = [
        {'titulo': 'Shrek', 'año': 2001},
        {'titulo': 'El viaje de Chihiro', 'año': 2002},
        {'titulo': 'Buscando a Nemo', 'año': 2003},
        {'titulo': 'Los Increíbles', 'año': 2004},
        {'titulo': 'Wallace y Gromit', 'año': 2005},
        {'titulo': 'Happy Feet', 'año': 2006},
        {'titulo': 'Ratatouille', 'año': 2007},
        {'titulo': 'WALL-E', 'año': 2008},
        {'titulo': 'Up', 'año': 2009},
        {'titulo': 'Toy Story 3', 'año': 2010},
        {'titulo': 'Rango', 'año': 2011},
        {'titulo': 'Valiente ', 'año': 2012},
        {'titulo': 'Frozen', 'año': 2013},
        {'titulo': 'Grandes Héroes', 'año': 2014}
    ]

    anio = int(input("Ingrese año (2001 a 2014) :"))
    peliculas = sorted(peliculas, key=lambda peli: peli['año'])
    pelicula = busqueda_binaria(peliculas, anio)
    print("La película no está en el catálogo!" if pelicula is None else pelicula)
