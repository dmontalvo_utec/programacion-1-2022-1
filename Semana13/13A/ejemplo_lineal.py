import random

import matplotlib.pyplot as plt
import numpy as np


def busqueda_lineal(lista, valor):
    for i in range(len(lista)):
        if lista[i] == valor:
            return True
    return False


def busqueda_lineal_n(lista, valor):
    """
    cuenta la cantidad de busquedas antes de encontrar el valor en la lista
    :param lista:
    :param valor:
    :return: encontrado, intento
    """
    n = 0
    for i in range(len(lista)):
        n += 1
        if lista[i] == valor:
            return True, n
    return False, n


def generate_list(n):
    return [random.randint(0, n) for i in range(n)]


if __name__ == '__main__':
    valor = random.randint(1, 20)
    lista = generate_list(20)
    print(f"Buscando {valor} en {lista}->", busqueda_lineal(lista, valor))

    valor = random.choice(lista)
    encontrado, veces = busqueda_lineal_n(lista, valor)
    print(f"Buscando {valor} en {lista}->", encontrado, f"{veces} busquedas")

    results = np.zeros((1000, 3), np.int16)
    for i in range(1, 1000):
        lista = generate_list(i)
        e = random.randint(0, i)  # dato a buscar
        results[i][0] = i  # tamaño lista
        results[i][1] ,results[i, 2] = busqueda_lineal_n(lista, e)

    plt.plot(results[:,0:1],results[:,2:3],'go')
    plt.ylabel("Intentos")
    plt.xlabel("Tamaño lista")
    plt.show()
