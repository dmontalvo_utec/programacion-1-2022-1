"""
Escribir un programa que permita ingresar n datos a un diccionario:
el nombre de un alumno es la clave y el código de estudiante es el valor.
Luego, ordenar el diccionario basado en el código de estudiante.
"""

estudiantes = {}


def burbuja(lista, estudiantes):
    n_iter = 0
    for tope in range(len(lista) - 1, 0, -1):
        for i in range(tope):
            n_iter += 1
            if estudiantes[lista[i]] > estudiantes[lista[i + 1]]:
                temp = lista[i]
                lista[i] = lista[i + 1]
                lista[i + 1] = temp
    return n_iter


if __name__ == '__main__':
    n = int(input("Ingrese la cantidad de estudiantes: "))

    for i in range(n):
        nombre = input("Ingrese el nombre del estudiante: ")
        codigo = int(input("Ingrese el codigo del estudiante: "))
        estudiantes[nombre] = codigo

    nombres = [nombre for nombre in estudiantes]
    burbuja(nombres, estudiantes)
    nuevoDict = {}
    for nombre in nombres:
        nuevoDict[nombre] = estudiantes[nombre]

    print(nuevoDict)
