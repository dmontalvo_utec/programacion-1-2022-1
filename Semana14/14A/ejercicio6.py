"""
Escribir un programa que tome una lista de matrices cuadradas
y ordena la lista basado en la suma de la diagonal principal de la matriz..



"""


def sumar_diagonal(matriz):
    suma = 0
    for i in range(len(matriz)):
        suma += matriz[i][i]
    return suma


def burbuja(lista):
    for tope in range(len(lista) - 1, 0, -1):
        for i in range(tope):
            if sumar_diagonal(lista[i]) > sumar_diagonal(lista[i + 1]):
                lista[i], lista[i + 1] = lista[i + 1], lista[i]
    return lista


if __name__ == '__main__':
    matrices = [[[3, 3, 3],
                 [3, 3, 3],
                 [3, 3, 3]],
                [[1, 1, 1],
                 [1, 1, 1],
                 [1, 1, 1]],
                [[2, 2, 2],
                 [2, 2, 2],
                 [2, 2, 2]]
                ]

    resultado = [[[1, 1, 1],
                  [1, 1, 1],
                  [1, 1, 1]],
                 [[2, 2, 2],
                  [2, 2, 2],
                  [2, 2, 2]],
                 [[3, 3, 3],
                  [3, 3, 3],
                  [3, 3, 3]]]
    burbuja(matrices)
    print(matrices)
