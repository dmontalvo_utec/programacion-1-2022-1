import random

import matplotlib.pyplot as plt
import numpy as np


# 5 8 4 6 3 2   (n=6)
def burbuja(lista):
    for i in range(len(lista) - 1, 0, -1):
        for j in range(i):
            if lista[j] > lista[j + 1]:
                lista[j], lista[j + 1] = lista[j + 1], lista[j]


def burbuja_n(lista):
    n = 0
    for i in range(len(lista) - 1, 0, -1):
        for j in range(i):
            n += 1
            if lista[j] > lista[j + 1]:
                lista[j], lista[j + 1] = lista[j + 1], lista[j]
    return n


def generar_lista(n):
    return [random.randint(0, n) for i in range(n)]


def complejidad():
    iteraciones = 1000
    results = np.zeros((iteraciones+1, 2), np.int64)
    for i in range(1, iteraciones):
        lista = generar_lista(i)
        results[i-1][0] = i  # tamaño lista
        results[i-1][1] = burbuja_n(lista)

    plt.plot(results[:, 0:1], results[:, 1:2], 'go')
    plt.ylabel("Comparaciones")
    plt.xlabel("Tamaño lista")
    plt.show()
    print("Finalizado")


if __name__ == '__main__':
    lista = generar_lista(5)
    print(lista)
    print("Ordenanding ..")
    burbuja(lista)
    print(lista)

    print("*" * 200)
    lista = generar_lista(20)
    print(lista)
    print("Ordenanding ..")
    burbuja(lista)
    print(lista)

    print("*" * 200)
    lista = generar_lista(50)
    print(lista)
    print("Ordenanding ..")
    burbuja(lista)
    print(lista)

    complejidad()
