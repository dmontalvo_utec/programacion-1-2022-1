import unittest

import ejercicio6


class MyTestCase(unittest.TestCase):

    def test_1(self):
        matrices = [[[3, 3, 3], [3, 3, 3], [3, 3, 3]], [[1, 1, 1], [1, 1, 1], [1, 1, 1]],
                    [[2, 2, 2], [2, 2, 2], [2, 2, 2]]]
        resultado = [[[1, 1, 1], [1, 1, 1], [1, 1, 1]], [[2, 2, 2], [2, 2, 2], [2, 2, 2]],
                     [[3, 3, 3], [3, 3, 3], [3, 3, 3]]]
        self.assertEqual(resultado, ejercicio6.burbuja(matrices))
