"""
Escribir un programa que reciba n strings, los guarde en una lista y luego
los ordene basado en la cantidad de vocales de los strings.
"""


def contar_vocales(palabra):
    vocales = 0
    for c in palabra:
        if c.lower() in ['a', 'e', 'i', 'o', 'u']:
            vocales += 1
    return vocales


def burbuja(lista):
    for tope in range(len(lista) - 1, 0, -1):
        for i in range(tope):
            if contar_vocales(lista[i]) > contar_vocales(lista[i + 1]):
                lista[i], lista[i + 1] = lista[i + 1], lista[i]


if __name__ == '__main__':
    num = int(input("Ingrese cantidad de strings: "))
    strings = []

    for i in range(num):
        strings.append(input("Ingrese una palabra: "))

    print(strings)
    print("Ordenando ...")
    burbuja(strings)
    print(strings)
