nombres = [
    "airton",
    "andy",
    "boris",
    "edwar",
    "esteban",
    "frank",
    "jonathan",
    "jose",
    "juan",
    "lidher",
    "liz",
    "luis"
]


def ordenar(lista):
    for i in range(len(lista) - 1, 0, -1):
        for j in range(i):
            if len(lista[j]) > len(lista[j + 1]):
                lista[j], lista[j + 1] = lista[j + 1], lista[j]


if __name__ == '__main__':
    print(nombres)
    print("ordenando")
    ordenar(nombres)
    print(nombres)
