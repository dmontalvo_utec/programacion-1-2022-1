import random

import matplotlib.pyplot as plt
import numpy as np


def merge(lista, left, right):
    i = j = k = 0
    while i < len(left) and j < len(right):
        if left[i] < right[j]:
            lista[k] = left[i]
            i += 1
        else:
            lista[k] = right[j]
            j += 1
        k += 1

    while i < len(left):
        lista[k] = left[i]
        i += 1
        k += 1

    while j < len(right):
        lista[k] = right[j]
        j += 1
        k += 1


def merge_n(lista, left, right, n):
    i = j = k = 0
    while i < len(left) and j < len(right):
        n += 1
        if left[i] < right[j]:
            lista[k] = left[i]
            i += 1
        else:
            lista[k] = right[j]
            j += 1
        k += 1

    while i < len(left):
        n += 1
        lista[k] = left[i]
        i += 1
        k += 1

    while j < len(right):
        n += 1
        lista[k] = right[j]
        j += 1
        k += 1

    return n


def merge_sort(lista):
    if len(lista) > 1:
        mid = len(lista) // 2
        izquierda = lista[:mid]
        derecha = lista[mid:]
        merge_sort(izquierda)
        merge_sort(derecha)
        merge(lista, izquierda, derecha)


def merge_sort_n(lista, n):
    if len(lista) > 1:
        mid = len(lista) // 2
        izquierda = lista[:mid]
        derecha = lista[mid:]
        n = merge_sort_n(izquierda, n + 1)
        n = merge_sort_n(derecha, n + 1)
        n = merge_n(lista, izquierda, derecha, n + 1)
        return n
    else:
        return n


def generar_lista(n):
    return [random.randint(0, n) for i in range(n)]


def complejidad():
    iteraciones = 1000
    results = np.zeros((iteraciones + 1, 2), np.int64)
    for i in range(1, iteraciones):
        lista = generar_lista(i)
        results[i - 1][0] = i  # tamaño lista
        results[i - 1][1] = merge_sort_n(lista, 0)

    plt.plot(results[:, 0:1], results[:, 1:2], 'go')
    plt.ylabel("Comparaciones")
    plt.xlabel("Tamaño lista")
    plt.show()
    print("Finalizado")


if __name__ == '__main__':
    print("*" * 200)
    lista = generar_lista(10)
    print(lista)
    print("Ordenanding ..")
    merge_sort(lista)
    print(lista)

    print("*" * 200)
    lista = generar_lista(20)
    print(lista)
    print("Ordenanding ..")
    merge_sort(lista)
    print(lista)

    complejidad()
