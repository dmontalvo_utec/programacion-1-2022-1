import random


def create_base(data):
    """
    creating data base
    :param data:
    :return:
    """
    print("**** GESTOR DE BROADCAST ****\n")
    print(f"[2] Crear base")
    print(f"Creando base de números de prueba...")

    bases = data['bases']
    if len(bases.keys()) == 0:
        current = "BASE_1"
    else:
        last = sorted(list(bases.keys()))[-1]
        current = "BASE_" + str(int(last[-1]) + 1)

    bases[current] = {
        "numbers": [random_number() for i in range(0, 10)]
    }


def random_number():
    return "".join(["9"] + [str(random.randint(0, 9)) for i in range(random.randint(1, 5))])


def buscar(data, numero):
    bases = data['bases']
    print(f"No se pudo hallar el número {numero} en {len(bases)} bases de números")


def search_number(data):
    print("**** GESTOR DE BROADCAST ****\n")
    print(f"[4] Buscando número")

    numero = input(f"Ingrese número de teléfono a buscar : ")
    buscar(data, numero)
