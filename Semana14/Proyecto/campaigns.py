import datetime
import os
import random


def create_campaign(data):
    """
    Creating simple campaign
    :param data:
    :return:
    """
    print("**** GESTOR DE BROADCAST ****\n")
    print(f"[1] Crear campaña")
    print(f"Creando campaña de prueba .... ")

    campaigns = data['campaigns']
    if len(campaigns.keys()) == 0:
        current = "SMS1"
    else:
        last = sorted(list(campaigns.keys()))[-1]
        current = "SMS" + str(int(last[-1]) + 1)

    campaigns[current] = {
        "text": "Mensaje de prueba",
        "variables": []
    }


def select_campaign(campaigns):
    return random.choice(list(campaigns.items()))


def select_base(bases):
    return random.choice(list(bases.items()))


def create_log(campaign_name, base_number, messages):
    datestr = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
    os.mkdir("logs") if not os.path.exists("logs") else None
    file = open(f"logs/{campaign_name}_{datestr}.log", "w", encoding="utf8")
    file.write(f"Ejecutando Campaña [{campaign_name}] con base [{base_number}]\n")
    file.write("-" * 100 + "\n")
    file.write(f"{len(messages)} mensajes enviados\n")
    file.write("-" * 100 + "\n")
    file.writelines(messages)
    file.close()


def send_campaign(data):
    campaigns = data['campaigns']
    bases = data['bases']
    print("**** GESTOR DE BROADCAST ****\n")
    print(f"[3] Lanzar campaña")

    campaign = select_campaign(campaigns)
    base = select_base(bases)
    messages = []
    for number in base[1]['numbers']:
        messages.append(f"{number.strip()} , {campaign[1]['text'].strip()}\n")

    create_log(campaign[0], base[0], messages)
