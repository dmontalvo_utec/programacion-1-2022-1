import json
import os

import bases
import campaigns


def loadData():
    if os.path.exists("data.json"):
        data_dict = json.load(open("data.json", "r"))
        if "campaigns" in data_dict.keys() and "bases" in data_dict.keys():
            return data_dict
        else:
            return Exception("Error in data.json")
    else:
        return {
            "campaigns": {},
            "bases": {}
        }


def saveData(data):
    with open("data.json", "w") as dataFile:
        return json.dump(data, dataFile)


def menu():
    print("**** GESTOR DE BROADCAST ****\n")
    print("[1] Crear campaña")
    print("[2] Cargar base de números")
    print("[3] Lanzar campaña")
    print("[4] Buscar número")
    print("[5] Créditos")
    print("[6] Salir")
    opcion = input("Ingrese una opción: ")
    return opcion


def clear_console():
    """
    Clear console
    :return: None
    """
    os.system('cls' if os.name == 'nt' else 'clear')


def show_credits():
    print("**** GESTOR DE BROADCAST ****\n")

    print(f"Dev Team\ns")
    print(f"* Diego Montalvo [dmontalvo.m@utec.edu.pe]\n")


def call_module(op, data):
    clear_console()
    if op == "1":
        campaigns.create_campaign(data)
        saveData(data)
    elif op == "2":
        bases.create_base(data)
        saveData(data)
    elif op == "3":
        campaigns.send_campaign(data)
    elif op == "4":
        bases.search_number(data)
    elif op == "5":
        show_credits()
    else:
        show_error(404, "Oción inválida o no soportada")

    pause()


def pause():
    input("Presione una tecla para continuar ...")


def show_error(code, message):
    print("**** GESTOR DE BROADCAST ****\n")
    print(f"Error {code}. {message}\n")


if __name__ == '__main__':
    data = loadData()
    while True:
        op = menu()
        if op == "5":
            break
        call_module(op, data)

    saveData(data)
    clear_console()
    print("Vuelva pronto!")
