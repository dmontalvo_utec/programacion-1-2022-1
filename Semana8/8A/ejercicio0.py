"""
Ejercicio 1
Dado una lista de valores, escriba un programa que permita identificar si un elemento se repite varias veces,
luego proceder a dejar solo la primera ocurrencia del elemento y eliminar el resto de apariciones.

"""
import random


def contar_y_eliminar(lista, valor):
    print(valor, "se repite", lista.count(valor), "veces")


def generarLista(size):
    lista = []
    for i in range(size):
        lista.append(random.randint(0, 10))
    return lista


if __name__ == '__main__':
    size = int(input("Ingrese tamaño de la lista : "))
    lista = generarLista(size)
    print("Lista generada")
    print(lista)

    valor = int(input("Ingrese valor a eliminar : "))
    contar_y_eliminar(lista, valor)

    print("Nueva lista")
    print(lista)
