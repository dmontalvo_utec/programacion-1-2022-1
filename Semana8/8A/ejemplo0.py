#listas
l1 = [1, 2, 3, 4, 5, 6]
l2 = [1, "2", "tres", True * 4, 4, 4]

print(l1)
print(l2)

#id's
print(id(l1))
print(id(l1[0]))
print(id(l2))
print(id(l2[0]))
#indices
print(l1[2:4])
#indices negativos
print(l1[-3:-1])
print(l1[-1:-3])

#strings vs listas

#lista listas