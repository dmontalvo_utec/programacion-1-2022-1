"""
Sin crear una lista adicional, elimine todos los elementos duplicados de una lista.

	input: [5, 8, 4, 7, 6, 2, 1, 9, 8, 4, 3, 2, 8]
	output: [5, 8, 4, 7, 6, 2, 1, 9, 3]

"""

import random


def generarLista(size):
    lista = []
    for i in range(size):
        lista.append(random.randint(0, 10))
    return lista


if __name__ == '__main__':
    size = int(input("Ingrese tamaño de la lista : "))
    lista = generarLista(size)
    print("Lista generada")
    print(lista)
