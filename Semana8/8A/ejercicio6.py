"""
Sin recorrer de inicio a fin reiterativamente una de las listas,
encuentre la diferencia (elementos que están en la primera lista pero no en la segunda) entre dos listas.
Dentro del resultado, no mostrar valores duplicados.

"""


import random


def generarLista(size):
    lista = []
    for i in range(size):
        lista.append(random.randint(0, 10))
    return lista


if __name__ == '__main__':
    size = int(input("Ingrese tamaño de la lista : "))
    lista_1 = generarLista(size)
    lista_2 = generarLista(size)
    print("Lista generada")
    print(lista)
