"""
El programa muestra cómo se leen datos desde el teclado y se adicionan a una lista,
luego el programa imprime los elementos de la lista utilizando varias formas de recorrido:

Utilizando el while
Utilizando el for con colecciones
Utilizando el for con rangos.

"""

num = int(input("Numero de elementos :"))
lista = []
for cont in range(1, num + 1, 1):
    dato = int(input("Dato %d: " % (cont)))
    lista.append(dato)
print(lista)
# ---- imprimimos la lista utilizando el while
print("Imprimimos los elementos de la lista utilizando while")
i = 0
while i < len(lista):
    print(lista[i], " ", end="")
    i = i + 1
print("\nImprimimos los elementos de la lista con el for ")
for elemento in lista:
    print(elemento, " ", end="")
print("\nImprimimos los elementos de la lista con el for con rango")
for i in range(0, len(lista), 1):
    print(lista[i], " ", end="")
