"""
Vecino más fuerte: encuentre el máximo para cada par de elementos continuos de una lista.

	input: [2, 4, 5, 2, 3, 8, 5, 1, 4]
	output: [4, 5, 5, 3, 8, 8, 5, 4]

"""

if __name__ == '__main__':
    lista = [2, 4, 5, 2, 3, 8, 5, 1, 4]
