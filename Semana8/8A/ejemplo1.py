# suma de listas
padres = ["mamá", "papá"]
abuelos = ["abuelo", "abuela"]
otros = ["tio", "tia"]

print(padres + abuelos + otros + ["hijo"])

# append

familia = []
familia.append("mamá")
familia.append("papá")
familia.append(abuelos)
print(familia)

# in y not in

print("mamá" in familia)
print("abuela" in familia)
print("abuelo" not in familia)

# unpack
mama, papa, resto = familia
print(mama)
print(resto)

# iteracion (simple, range y emumerada)
for fam in resto:
    familia.append(fam)

print(familia)

for idx, familiar in enumerate(familia):
    print(f"Familiar N° {idx} : {familiar}")



