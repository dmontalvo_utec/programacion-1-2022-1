# in - index
lista_letras = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'h', 'i', 'j']

# del, remove , pop

lista_letras = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'h', 'i', 'j']
del lista_letras[3]
print(lista_letras)
del lista_letras[:2]
print(lista_letras)

eliminado = lista_letras.remove('h')
print(lista_letras)

eliminado = lista_letras.pop()
print(eliminado)
print(lista_letras)
print(lista_letras.pop(0))
print(lista_letras)
