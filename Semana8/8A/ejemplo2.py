# WHILE para escribir en listas, FOR para leer

lineas = []
print("Ingrese frases!")
print("Ingrese un texto vacio para finalizar!")

linea = input("Frase : ")
while linea != "":
    lineas.append(linea)
    linea = input("Siguiente frase : ")

print("Sus frases fueron : ")

for frase in lineas:
    print(frase)
