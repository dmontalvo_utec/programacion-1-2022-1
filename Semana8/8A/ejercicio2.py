"""
Sin recorrer explícitamente la lista de inicio a fin,
encuentre todas las posiciones en las que se encuentra un elemento determinado en una lista.

	input: [8, 4, 3, 2, 1, 5, 6, 2, 1, 4, 5, 8, 9]
		2
	output: 3 7

"""

if __name__ == '__main__':
    lista = [8, 4, 3, 2, 1, 5, 6, 2, 1, 4, 5, 8, 9]
