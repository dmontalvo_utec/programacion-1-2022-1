# El simbolo = en listas es REFERENCIA!!
print("El igual")

lista_numeros = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
lista_puntero = lista_numeros

print(lista_puntero)

lista_numeros[0] *= 2
lista_numeros[2] *= 2
lista_numeros[4] *= 2

print(lista_puntero)

# copiar lista! slicing y copy
print("Copiando")

lista_copia = lista_numeros[1:9]
print(lista_copia)

lista_copia_2 = lista_numeros.copy()
print(lista_copia_2)
lista_numeros[0] = 100
print(lista_numeros)
print(lista_copia_2)

# append e insert
print("Append e insert")
print(lista_numeros)
lista_numeros.append(100)
lista_numeros.insert(0, 2000)
print(lista_numeros)


