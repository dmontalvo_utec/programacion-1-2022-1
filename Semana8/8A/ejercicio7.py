"""
Un tablero de ajedrez puede ser representado por una matriz de 8 x 8.
Asuma que el tablero esta vacío (lleno de 0s).

Posteriormente, solicite una posición inicial en el tablero.
Finalmente, simule a que posiciones (marcando con un 1)
se podría mover desde esa posición: una torre (a), un alfil (b) y una reina (c.).

Se sugiere implementar una función para cada pieza.

"""


def torre(tablero, x, y):
    tablero[x][y] = 'a'


def alfil(tablero, x, y):
    tablero[x][y] = 'b'


def torre(tablero, x, y):
    tablero[x][y] = 'c'


def generarTablero():
    tablero = []
    for i in range(8):
        tablero.append([0] * 8)
    return tablero


def imprimirTablero(tablero):
    for fila in tablero:
        print(fila)


if __name__ == '__main__':
    tablero = generarTablero()
    imprimirTablero(tablero)
