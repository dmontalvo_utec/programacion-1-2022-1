"""
Sin crear una lista adicional y sin usar slicing, invierta una lista.
	input: [1, 2, 3, 4, 5]
	output: [5, 4, 3, 2, 1]
"""

if __name__ == '__main__':
    lista = [1, 2, 3, 4, 5]
    print("input", lista)
    print("output", lista)
