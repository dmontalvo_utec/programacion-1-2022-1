"""
Arbolitos - bucles
Leer 1 caracter, nro de figuras, nro de filas cada figura(triangulo invertido)

Validar, nro de figuras y nro de filas mayor a cero.


"""

c = input("Caracter : ")

figuras = int(input("Nro de figuras : "))
while figuras <= 0:
    figuras = int(input("Nro de figuras : "))

filas = int(input("Nro de filas : "))
while filas <= 0:
    filas = int(input("Nro de filas : "))

for i in range(figuras):
    for j in range(filas):
        for k in range(filas - j ):
            print(c, end=" ")
        print()
    print()
