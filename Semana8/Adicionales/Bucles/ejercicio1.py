"""
Lea un dato N y dibuje la siguiente figura

n=8
# # # # # # # #
# *         * #
#   *     *   #
#     * *     #
#     * *     #
#   *     *   #
# *         * #
# # # # # # # # 

"""

n = int(input("Ingrese N : "))

for i in range(1, n + 1):
    for j in range(1, n + 1):
        if i == 1 or i == n or j == 1 or j == n:
            print("#", end=" ")
        elif i == j or i == n - j + 1:
            print("*", end=" ")
        else:
            print(" ", end=" ")
    print()
