# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import random
import time

import PySimpleGUI as sg

grupos = [1, 4, 5, 6, 7]
seleccionados = []


def select_grupo():
    if len(seleccionados) == len(grupos):
        exit(0)

    while True:
        grupo = random.choice(grupos)
        if grupo not in seleccionados:
            seleccionados.append(grupo)
            break

    return f"Grupo {grupo}"


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.


# Press the green button in the gutter to run the script.
if __name__ == '__main__':

    nombre_grupo = "??"

    layout = [
        [sg.Text("Proyecto 2. Sorteo de grupos", font=("Any 20"), justification="center", size=(50, 1))],
        [sg.vcenter([sg.Button("Elegir victima", font=("Any 15"))])],
        [sg.HSeparator()],
        [sg.Text(nombre_grupo, font=("Any 35"), key='grupo_elegido', size=(30, 2), justification="center")]
    ]

    window = sg.Window(title="Proyecto 2", layout=layout, margins=(50, 50))

    while True:  # Event Loop
        event, values = window.read()
        if event in (None, 'Exit'):
            break
        if event == "Elegir victima":
            grupo = select_grupo()
            for i in range(3, 0, -1):
                window["grupo_elegido"].update(f"{i}...")
                window.refresh()
                time.sleep(1)
            window["grupo_elegido"].update(grupo)
